﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class RectExtensions
{
    public static bool ContainsRect(this Rect r, Rect rect)
    {
		return r.ContainsPoint(new Vector2(rect.xMin, rect.yMin)) && r.ContainsPoint(new Vector2(rect.xMax, rect.yMax));
    }

	/// <summary>
	/// If the point dimensions are less than -or equal- to the min / max dimensions of the rect, returns true.
	/// </summary>
	public static bool ContainsPoint(this Rect r, Vector2 point)
	{
		return r.xMin <= point.x && r.xMax >= point.x && r.yMin <= point.y && r.yMax >= point.y;
	}
}
