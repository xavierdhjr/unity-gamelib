﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public static class ColorExtensions
{
    /// <summary>
    /// Converts RGB values with range 0 - 255 into percentage values
    /// and returns the corresponding color. 
    /// </summary>
    public static Color WithRGB(this Color c, int r, int g, int b)
    {
        return new Color((float)r / 255f, (float)g / 255f, (float)b / 255f, c.a);
    }

    public static Color WithAlpha(this Color c, float alpha)
    {
        c.a = alpha;
        return c;
    }

    public static string ToHexString(this Color c)
    {
        int r = (int)(c.r * 255);
        int g = (int)(c.g * 255);
        int b = (int)(c.b * 255);

        return r.ToString("X2") + g.ToString("X2") + b.ToString("X2");
    }
}
