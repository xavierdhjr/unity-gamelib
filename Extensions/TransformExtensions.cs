﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class TransformExtensions
{
	/// <summary>
	/// Traverses up the Transform hierarchy until component of type T is found.
	/// </summary>
	public static T TraverseHierarchyFor<T>(this Transform transform, bool excludeSelf = false) where T: Component
	{
		int x = 0;
		Transform t = transform;

		if(excludeSelf)
		{
			t = transform.parent;
		}

		while(t != null)
		{
			T component = t.GetComponent<T>();
			if(component != null)
				return component;

			x++;
			t = t.parent;
		}
		return null;
	}

	static List<T> RecurseDownFor<T>(Transform transform) where T:Component
	{
		List<T> _components = new List<T>();

		T[] components = transform.GetComponents<T>();
		
		_components.AddRange(components);

		
		foreach(Transform child in transform)
		{
			_components.AddRange(RecurseDownFor<T>(child));
		}
		
		return _components;
	}
	
	public static T[] TraverseDownFor<T>(this Transform transform, bool excludeSelf = false) where T: Component
	{

		List<T> componentsList = RecurseDownFor<T>(transform);

		if(excludeSelf && componentsList.Count > 0 && componentsList[0].transform == transform)
		{
			componentsList.RemoveAt(0);
		}

		T[] components = new T[componentsList.Count];
		for(int i = 0; i < components.Length; ++i)
		{

			components[i] = componentsList[i];
		}





		return components;
	}

	static Transform RecurseDownForChildWithName(Transform t, string name)
	{
		Transform currentTransform = t;
		
		if(currentTransform != null)
		{
			if(currentTransform.name == name)
				return currentTransform;

			foreach(Transform child in currentTransform)
			{
				Transform found = RecurseDownForChildWithName(child, name);
				if(found != null)
					return found;
			}

		}

		return null;
	}

	public static Transform TraverseDownForChildWithName(
		this Transform transform,
		string name)
	{
		return RecurseDownForChildWithName(transform, name);
	}

	public static string GetTransformPath(this Transform t, Transform relativeTo = null)
	{
		Transform p = t.parent;
		string path = t.name;

		while(p != null && p != relativeTo)
		{
			path = p.name + "/" + path;
			p = p.parent;
		}

		return path;
	}

	/// <summary>
	/// Searches a transform's hierarchy using the specified '/' separated path
	/// </summary>
	public static Transform GetTransformByPath(this Transform top, string path)
	{
		string[] transformNames = path.Split(new char[]{ '/' }, System.StringSplitOptions.RemoveEmptyEntries);
		Transform current = top;

		for(int i = 0; i < transformNames.Length; ++i)
		{
			Transform t = current.Find(transformNames[i]);
			if(t != null)
				current = t;
		}
		return current;
	}

	public static Transform FindTransformByPath(string path)
	{
		string[] transformNames = path.Split(new char[] { '/' }, System.StringSplitOptions.RemoveEmptyEntries);
		GameObject o = GameObject.Find(transformNames[0]);

		if (o == null)
			return null;

		Transform current = o.transform;

		for (int i = 1; i < transformNames.Length; ++i)
		{
			Transform t = current.Find(transformNames[i]);
			if (t != null)
				current = t;
			else
				return null;
		}
		return current;
	}
}