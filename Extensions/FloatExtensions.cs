﻿using UnityEngine;
using System.Collections;

public static class FloatExtensions
{
    public static string ToTime(this float num)
    {
		float originalNum = num;
		
		if(originalNum < 1)
		{
			return num.ToString("0.00") + "s";
		}

		int days = (int)(num / 60 / 60 / 24);

		num %= 60 * 60 * 24;

		int hours = (int)(num / 60 / 60);

		num %= 60 * 60;

		int minutes = (int)(num / 60);
		
		num %= 60;

		string v = "";

        if (days > 0)
            v += days + "d ";
        if (hours > 0)
			v += hours + "h ";
        if (minutes > 0)
			v += minutes + "m ";

		int seconds = Mathf.FloorToInt(num);

		if(seconds > 0)
        	return v + Mathf.Floor(num) + "s";
		return v;
    }
}
