﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

using System.Linq;



public static class GameObjectExtensions

{

	static Assembly currentAssembly
	{
		get
		{
			return Assembly.GetCallingAssembly();
		}
	}

	public static T[] FindGameObjectsOfInterface<T>()
	{
		if (!typeof(T).IsInterface) throw new SystemException("Specified type is not an interface!");

		Type[] typesWithInterface = currentAssembly.GetTypes().Where(x => x.GetInterfaces().Contains(typeof(T))).ToArray();

		List<T> interfaces = new List<T>();

		for(int i = 0; i < typesWithInterface.Length; ++i)
		{
			UnityEngine.Object[] objs = GameObject.FindObjectsOfType(typesWithInterface[i]);
			
			Debug.Log(objs.Length + " " + typesWithInterface[i]);
			for(int k = 0; k < objs.Length; ++k)
			{
				Debug.Log(objs[k]);
				interfaces.Add((T)(object)objs[k]);
			}
		}

		return interfaces.ToArray();


		/*
		var mObjs = GameObject.FindObjectsOfType<MonoBehaviour>();
		for(int i =0; i < mObjs.Count(); ++i)
		{
			Debug.Log(mObjs[i]);
		}
		*/
		//return mObjs.Where(x => x.gameObject.GetInterface<T>() != null).Select(x => x.gameObject.GetInterface<T>()).ToArray();

		//return (from a in mObjs where a.GetType().GetInterfaces().Any(k => k == typeof(T)) select (T)(object)a).ToArray();
	}

	/// <summary>

	/// Returns all monobehaviours (casted to T)

	/// </summary>

	/// <typeparam name="T">interface type</typeparam>

	/// <param name="gObj"></param>

	/// <returns></returns>

	public static T[] GetInterfaces<T>(this GameObject gObj)

	{
		if (!typeof(T).IsInterface) throw new SystemException("Specified type is not an interface!");

		var mObjs = gObj.GetComponents<MonoBehaviour>();

		return (from a in mObjs where a.GetType().GetInterfaces().Any(k => k == typeof(T)) select (T)(object)a).ToArray();

	}



	/// <summary>

	/// Returns the first monobehaviour that is of the interface type (casted to T)

	/// </summary>

	/// <typeparam name="T">Interface type</typeparam>

	/// <param name="gObj"></param>

	/// <returns></returns>

	public static T GetInterface<T>(this GameObject gObj)

	{

		if (!typeof(T).IsInterface) throw new SystemException("Specified type is not an interface!");

		return gObj.GetInterfaces<T>().FirstOrDefault();

	}



	/// <summary>

	/// Returns the first instance of the monobehaviour that is of the interface type T (casted to T)

	/// </summary>

	/// <typeparam name="T"></typeparam>

	/// <param name="gObj"></param>

	/// <returns></returns>

	public static T GetInterfaceInChildren<T>(this GameObject gObj)

	{

		if (!typeof(T).IsInterface) throw new SystemException("Specified type is not an interface!");

		return gObj.GetInterfacesInChildren<T>().FirstOrDefault();

	}



	/// <summary>

	/// Gets all monobehaviours in children that implement the interface of type T (casted to T)

	/// </summary>

	/// <typeparam name="T"></typeparam>

	/// <param name="gObj"></param>

	/// <returns></returns>

	public static T[] GetInterfacesInChildren<T>(this GameObject gObj)

	{

		if (!typeof(T).IsInterface) throw new SystemException("Specified type is not an interface!");



		var mObjs = gObj.GetComponentsInChildren<MonoBehaviour>();



		return (from a in mObjs where a.GetType().GetInterfaces().Any(k => k == typeof(T)) select (T)(object)a).ToArray();

	}

}