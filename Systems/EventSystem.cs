﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class EventSystem
{
	static EventSystem _system;
    static string _lastEvent;

    public static string LastEvent
    {
        get { return _lastEvent; }
    }

	public static EventSystem Get
	{
		get{
            if (_system == null)
                _system = new EventSystem();
			return _system; 
		}
	}

	Dictionary<string, List<EventHandler>> _events = new Dictionary<string, List<EventHandler>>();
	Dictionary<string, List<object>> _listeners = new Dictionary<string, List<object>>();

	public delegate void EventHandler(object[] args);

	public void ClearAll()
	{
		_events.Clear();
		_listeners.Clear();
		_lastEvent = "";
	}

    public void ListenForAny(string[] events, EventHandler handler, object listener)
    {
        for(int i = 0; i < events.Length; ++i)
        {
            Listen(events[i], handler, listener);
        }
    }

	public void Listen(string event_name, EventHandler handler, object listener = null)
	{
		List<EventHandler> existingHandler;
		if(_events.TryGetValue(event_name, out existingHandler))
		{
			existingHandler.Add(handler);
		}else{
			_events.Add(event_name, new List<EventHandler>(){ handler });
		}

		List<object> existingListeners;
		if(_listeners.TryGetValue(event_name, out existingListeners))
		{
			existingListeners.Add(handler);
		}else{
			_listeners.Add(event_name, new List<object>(){ listener });
		}

	}

	public void StopListen(string event_name, EventHandler handler, object listener = null)
	{
		List<EventHandler> existingHandler;
		if(_events.TryGetValue(event_name, out existingHandler))
		{
			existingHandler.Remove(handler);
		}else{
			_events.Remove(event_name);
		}
		
		List<object> existingListeners;
		if(_listeners.TryGetValue(event_name, out existingListeners))
		{
			existingListeners.Remove(handler);
		}else{
			_listeners.Remove(event_name);
		}
	}

	public void Fire(string event_name, params object[] args)
	{

		List<EventHandler> existingHandler;
		if(_events.TryGetValue(event_name, out existingHandler))
		{
			// Index into handler list
			int handler_id = 0;

			foreach (EventHandler handler in existingHandler)
			{
				try
                {
                    // Set the last event to make it possible for one callback to handle multiple event types
                    _lastEvent = event_name;
					handler(args);
				}catch(System.Exception e)
				{
					List<object> listeners;
					Object o = null;
					if(_listeners.TryGetValue(event_name, out listeners))
					{
						if(o is Object)
							o = (Object)listeners[handler_id];

						listeners.Remove(handler_id);
					}

                    Log.Error("Error while executing event '" + event_name + "' on " + o, o);
                    Log.Exception(e);
				}

				handler_id++;
			}
			return;
		}
	}
}
