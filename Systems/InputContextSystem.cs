﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

/// <summary>
/// This singleton allows one to determine what objects the mouse is
/// currently positioned over at any given time, sorted by distance.
/// </summary>
public class InputContextSystem : MonoBehaviour 
{
    public enum ContextType
    {
        Normal,
        UICanvas
    }

	private class InputContext
	{
		public string name;
		public Camera camera;
		public GameObject mouseOverObject;
		public GameObject[] mouseOverObjects;
        public Vector3 position;
        public ContextType Type;

		public InputContext(string contextName, ContextType type)
		{
			name = contextName;
			mouseOverObjects = new GameObject[]{};
            Type = type;
		}

        public void RaycastFromInput(PlatformInput input)
        {
            if (Type == ContextType.Normal)
            {
                Ray ray = camera.ScreenPointToRay(input.CursorPosition());
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 1000, camera.cullingMask))
                {
                    mouseOverObject = hit.collider.gameObject;
                    position = hit.point;
                }
                else
                {
                    // Fixes an issue where a mouseOverObject that was not being hovered
                    // was still being used by client scripts. Now it gets set to null
                    // when nothing is being hovered.
                    mouseOverObject = null;
                    position = Vector3.zero;
                }

                // Sort hits in order of distance from the raycast.
                RaycastHit[] hits = SortList(Physics.RaycastAll(ray, 1000, camera.cullingMask));
                GameObject[] objs = new GameObject[hits.Length];

                for (int k = 0; k < hits.Length; ++k)
                    objs[k] = hits[k].collider.gameObject;

                mouseOverObjects = objs;
            }else if(Type == ContextType.UICanvas)
            {
                UnityEngine.EventSystems.EventSystem eventSystem = UnityEngine.EventSystems.EventSystem.current;
                List<UnityEngine.EventSystems.RaycastResult> results = new List<UnityEngine.EventSystems.RaycastResult>();
                UnityEngine.EventSystems.PointerEventData pointer = new UnityEngine.EventSystems.PointerEventData(eventSystem);
                pointer.position = input.CursorPosition();
                eventSystem.RaycastAll(pointer, results);

                GameObject[] objs = new GameObject[results.Count];
                for(int i = 0; i < results.Count; ++i)
                {
                    objs[i] = results[i].gameObject;
                    
                }

                if (objs.Length > 0)
                    mouseOverObject = objs[0];
                else
                    mouseOverObject = null;
                position = pointer.position;
                mouseOverObjects = objs;
            }
        }

        public Vector3 ViewportToWorld(Vector2 viewport)
        {
            if (Type == ContextType.Normal)
            {
                return camera.ViewportToWorldPoint(viewport);
            }

            // Canvas
            RectTransform rectTrans = camera.GetComponent<RectTransform>();
            return new Vector2(
                ((viewport.x * rectTrans.sizeDelta.x) - (rectTrans.sizeDelta.x * 0.5f)),
                ((viewport.y * rectTrans.sizeDelta.y) - (rectTrans.sizeDelta.y * 0.5f)));
        }

        public Vector3 WorldToViewport(Vector3 worldPoint)
        {
            return camera.WorldToViewportPoint(worldPoint);
        }


        RaycastHit[] SortList(RaycastHit[] raycasthits)
        {
            //copy to list
            List<RaycastHit> sortList = new List<RaycastHit>();
            foreach (RaycastHit hit in raycasthits)
            {
                sortList.Add(hit);
            }

            //run sort
            sortList.Sort(compare);

            RaycastHit[] retval = new RaycastHit[sortList.Count];

            //copy back to array
            for (int i = 0; i < sortList.Count; i++)
            {
                retval[i] = sortList[i];
            }

            return retval;

        }

        // comparison by distance, ascending order
        int compare(RaycastHit hitA, RaycastHit hitB)
        {
            if (hitA.distance > hitB.distance)
                return 1;
            else if (hitA.distance < hitB.distance)
                return -1;
            return 0;

        }

	}

    public const string CONTEXT_MAIN = "main";
    public const string CONTEXT_GUI = "gui";
    public const string CONTEXT_OBJECTS = "objects";

	Camera _guiCamera;
    Camera _mainCamera;

	PlatformInput _input;


	static List<InputContext> _contexts = new List<InputContext>();

    public static UIHitbox GetUIHitbox(string context = CONTEXT_GUI)
    {
        return GetHoverForContextWithComponent<UIHitbox>(context);
    }

	public static Vector3 ConvertPoint(Vector3 point, string from_context = CONTEXT_MAIN, string to_context = CONTEXT_GUI)
	{

        InputContext fromContext = _contexts.Find(x => x.name == from_context);
        InputContext toContext = _contexts.Find(x => x.name == to_context);

        Vector2 viewport = fromContext.WorldToViewport(point);
        return toContext.ViewportToWorld(viewport);
	}

	public static GameObject GetHoverForContext(string context = CONTEXT_MAIN)
	{
		InputContext inputContext = _contexts.Find(x => x.name == context);

		if(inputContext == null)
			return null;

		return inputContext.mouseOverObject;
	}

	public static Camera GetCameraForContext(string context = CONTEXT_MAIN)
	{
		InputContext inputContext = _contexts.Find(x => x.name == context);
		
		if(inputContext == null)
			return null;
		
		return inputContext.camera;
	}

    public static Vector3 GetHitPointForContext(string context = CONTEXT_MAIN)
    {

        InputContext inputContext = _contexts.Find(x => x.name == context);

        if (inputContext == null)
            return Vector3.zero;

        return inputContext.position;
    }

    public static T GetHoverForContextWithComponent<T>(string context = CONTEXT_MAIN) where T: Component
    {

        InputContext inputContext = _contexts.Find(x => x.name == context);

        if (inputContext == null)
            return null;

        foreach(GameObject g in inputContext.mouseOverObjects)
        {
            T component = g.GetComponent<T>();
            if (component != null)
                return component;
        }

        return null;
    }

    public static T GetContextComponentOnLayer<T>(int layer, string context = CONTEXT_MAIN) where T:Component
    {
        InputContext inputContext = _contexts.Find(x => x.name == context);

        if (inputContext == null)
            return null;

        foreach (GameObject g in inputContext.mouseOverObjects)
        {
            if (g.layer != layer)
                continue;

            T component = g.GetComponent<T>();
            if (component != null)
            {
                return component;
            }
        }

        return null;
    }

	public static List<GameObject> GetObjectsForContext(string context = CONTEXT_MAIN)
	{
		InputContext inputContext = _contexts.Find(x => x.name == context);
		
		if(inputContext == null)
			return null;
		
		return inputContext.mouseOverObjects.ToList ();
	}

	public static bool ContextContainsTypeAtAll(string context, System.Type type)
	{
		InputContext inputContext = _contexts.Find(x => x.name == context);

		for(int i = 0; i < inputContext.mouseOverObjects.Length; ++i)
		{
			if(inputContext.mouseOverObjects[i].GetComponent(type) != null)
				return true;
		}

		return false;
	}

	public static bool IsInContextAtAll(string context, GameObject obj)
	{
		
		InputContext inputContext = _contexts.Find(x => x.name == context);

		if(inputContext == null)
			return false;

		return inputContext.mouseOverObjects.Contains(obj);
	}

    public Camera GUICamera;

	public void Awake()
	{
        /*
         * Configuration
         */ 
		_contexts = new List<InputContext>();
        _mainCamera = Camera.main;
        _guiCamera = GUICamera;


        // Only works with one user's input
		_input = InputSystem.Get.Player1Input;

		_contexts.Add(new InputContext(CONTEXT_MAIN, ContextType.Normal)
		              {
			camera = _mainCamera
		});
		_contexts.Add(new InputContext(CONTEXT_GUI, ContextType.UICanvas)
		              {
			camera = _guiCamera
                      });

	}

	// Update is called once per frame
	public void Update () 
	{

		for(int i = 0; i < _contexts.Count; ++i)
		{
            if (_contexts[i].camera == null) continue;

            _contexts[i].RaycastFromInput(_input);
		}
	}

    RaycastHit[] SortList(RaycastHit[] raycasthits)
    {
        //copy to list
        List<RaycastHit> sortList = new List<RaycastHit>();
        foreach (RaycastHit hit in raycasthits)
        {
            sortList.Add(hit);
        }

        //run sort
        sortList.Sort(compare);

        RaycastHit[] retval = new RaycastHit[sortList.Count];

        //copy back to array
        for (int i = 0; i < sortList.Count; i++)
        {
            retval[i] = sortList[i];
        }

        return retval;

    }

    // comparison by distance, ascending order
    int compare(RaycastHit hitA, RaycastHit hitB)
    {
        if (hitA.distance > hitB.distance)
            return 1;
        else if (hitA.distance < hitB.distance)
            return -1;
        return 0;

    }

}
