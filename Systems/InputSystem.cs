﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InputSystem : MonoBehaviour
{
    public const int CURSOR_PRIMARY = 0;

    public const int FILTER_STANDARD = 1;
    public const int FILTER_CONSOLE = 2;

	static InputSystem _system;

	public static InputSystem Get
	{
		get
        {
            if (_system == null)
                _system = GameObject.FindObjectOfType<InputSystem>();
            return _system; 
        }
	}

	public enum ControllerSelection
	{
		XboxController,
        KeyboardController,
		IPadController
	}

    public PlatformInput Player1Input;
    public PlatformInput Player2Input;
    public PlatformInput Player3Input;
    public PlatformInput Player4Input;

    public Stack<int> ControlFilterStack = new Stack<int>();

    public int GetInputCount()
    {
        return 4;
    }

    public void PushFilter(int filter)
    {
        ControlFilterStack.Push(filter);
    }

    public int PopFilter()
    {
        return ControlFilterStack.Pop();
    }

    public int GetCurrentFilter()
    {
        return ControlFilterStack.Peek();
    }

    public PlatformInput GetPlayerInput(int player = 1)
    {
        switch(player)
        {
            case 1:
                return Player1Input;
            case 2:
                return Player2Input;
            case 3:
                return Player3Input;
            case 4:
                return Player4Input;
        }
        return Player1Input;
    }

	void Awake()
	{
        Player1Input = DetermineControlScheme(0, 1);
        Player2Input = DetermineControlScheme(0, 2);
        Player3Input = DetermineControlScheme(0, 3);
        Player4Input = DetermineControlScheme(0, 4);

        PushFilter(FILTER_STANDARD);
	}

	void Update()
	{
        Player1Input.UpdateInput(Time.deltaTime);
        Player2Input.UpdateInput(Time.deltaTime);
        Player3Input.UpdateInput(Time.deltaTime);
        Player4Input.UpdateInput(Time.deltaTime);
	}
	
	PlatformInput DetermineControlScheme(ControllerSelection selection, int player)
	{

        return new DefaultPlayerInput();

        /*
#if UNITY_EDITOR
        return new XboxControllerInput(player); 
#elif UNITY_IOS
		return new IPadInput();
#else
		return new XboxControllerInput(player); 
#endif*/

    }


}