﻿using UnityEngine;

/// <summary>
/// Used to show Particles on top of UI
/// </summary>
[ExecuteInEditMode]
public class SetRenderQueue : MonoBehaviour
{
	public int renderQueue = 3000;
	
	Material mMat;
	
	void Start ()
	{
		Renderer ren = GetComponent<Renderer>();
		
		if (ren == null)
		{
			ParticleSystem sys = GetComponent<ParticleSystem>();
			if (sys != null) ren = sys.GetComponent<Renderer>();
		}
		
		if (ren != null && ren.sharedMaterial != null)
		{

			mMat = new Material(ren.sharedMaterial);
			mMat.renderQueue = renderQueue;
			ren.material = mMat;
		}
	}

	void Update()
	{
		if(mMat != null)
		{
			mMat.renderQueue = renderQueue;
		}else{
			Start ();
		}
	}
	
	void OnDestroy () {
		if (Application.isPlaying)
		{
			if (mMat != null) Destroy(mMat);
		}
	}
}