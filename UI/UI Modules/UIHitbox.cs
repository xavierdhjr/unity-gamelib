﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class UIHitbox : UIShowHideListener 
{
    Collider _col;
	BaseController _scope;
	public bool BaseOnScope = true;

	public new void Setup()
    {
		base.Awake();
        if (UI == null)
        {
            return;
        }
		
		if (!UI.Shown)
			OnUIHidden(UI);
		else
			OnUIShown(UI);

		_scope = transform.TraverseHierarchyFor<BaseController>();

	}

    protected override void Awake()
    {
        Setup();
    }


    public override void OnUIShown(GameUI ui)
    {
        if (this == null)
            return;

		_col = GetComponent<Collider>();
        _col.enabled = true;

        Button _button = GetComponent<Button>();
		if (_button != null)
            _button.interactable = true;

        base.OnUIShown(ui);
    }

    public override void OnUIHidden(GameUI ui)
    {
        if (this == null)
            return;

		_col = GetComponent<Collider>();
        _col.enabled = false;

        Button _button = GetComponent<Button>();
		if (_button != null)
			_button.interactable = false;

        base.OnUIHidden(ui);
    }

	void OnDestroy()
	{
		if(UI != null)
            UI.StopListeningForShowHideEvents(this);
	}

	void Update()
	{
		if(BaseOnScope)
		{
			_col = GetComponent<Collider>();
			if(_col == null)
				return;
			if(_scope != null && _scope.ControllerGameUI != null)
				_col.enabled = _scope.ControllerGameUI.Shown;
		}
	}

}
