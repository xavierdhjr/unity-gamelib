﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Represents a module that deals with a GameUI in one way or another.
/// </summary>
public abstract class UIModuleBase : MonoBehaviour
{
    protected const int LABEL_HEIGHT = 17;
    protected const int BOX_HEIGHT = 24;
    protected const int BUTTON_HEIGHT = 18;

    public abstract GameUI UI { get; }


}
