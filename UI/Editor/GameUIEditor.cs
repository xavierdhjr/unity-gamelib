﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(GameUI),true)]
public class GameUIEditor : Editor {

	public override void OnInspectorGUI ()
	{
		GameUI ui = target as GameUI;

        if (Application.HasProLicense())
		    GUI.skin.box.normal.textColor = Color.white;
		if(ui.Shown)
			GUI.color = Color.green;
		else
			GUI.color = Color.red;
		GUILayout.Box(ui.Shown ? "Shown" : "Hidden",GUILayout.ExpandWidth(true));
		GUI.color = Color.white;
		GUI.skin.box.normal.textColor = Color.black;

		base.OnInspectorGUI ();
	}

}
