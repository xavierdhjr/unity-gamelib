﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class GameSettings
{
	public const bool DEBUG_LOGGING = false;

    public const string GAMEDATA_FILE = "game.data";

    public const string LAYER_DEFAULT = "Default";
    public const string LAYER_GUI = "UI";
    public const string LAYER_OBJECTS = "Default";

    public static string GAMEDATA_FILEPATH = Application.streamingAssetsPath + "/" + GameSettings.GAMEDATA_FILE;

	public enum Platform
	{
		Windows,
		Mac,
		Linux,
		Xbox,
		Steam,
		IOS,
		Android,
		Unknown
	}
	
	public static Platform CurrentPlatform
	{
		get
		{
#if UNITY_STANDALONE_OSX
			return Platform.Mac;
#elif UNITY_STANDALONE_WIN
			return Platform.Windows;
#elif UNITY_WEBPLAER
			return Platform.Unknown;
#elif UNITY_IOS
			return Platform.IOS;
#elif UNITY_ANDROID
			return Platform.Android;
#else
			return Platform.Unknown;
#endif
		}
	}

	public static void Log(string msg)
	{
		if(DEBUG_LOGGING)
			Debug.Log(System.DateTime.Now.ToLongTimeString() + " - " + msg);
	}

}
