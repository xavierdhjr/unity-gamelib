﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using System.Reflection;

public class ConsoleCommand : System.Attribute
{
    public string Command;
    public string Description;

    public ConsoleCommand(string command, string description = "")
    {
        Command = command;
        Description = description;
    }
}

public static class CommonConsoleCommands
{
    [ConsoleCommand("dingus")]
    public static void Dingus()
    {
        Log.Console("You are a dingus.");
    }

    [ConsoleCommand("mute_sound")]
    public static void MuteSound()
    {
        AudioSource[] sources = GameObject.FindObjectsOfType<AudioSource>();
        foreach(AudioSource src in sources)
        {
            src.volume = 0;
        }
        Log.Console("Sound muted. Restart game to restore audio to normal.");
    }

    [ConsoleCommand("scene", "Loads a scene")]
    public static void LoadScene(string name)
    {
        Application.LoadLevel(name);
    }

    [ConsoleCommand("quit", "Quits the application")]
    public static void Quit()
    {
        Application.Quit();
    }


    [ConsoleCommand("pause", "Pauses the application if in the editor.")]
    public static void Pause()
    {
        Debug.Break();
    }


}


public class ConsoleController : BaseController
{
    [ConsoleCommand("search", "Searches for a command containing the given string")]
    public static void Search(string command)
    {
        Dictionary<string, CommandInfo> cmnds = ConsoleController.Get.Commands;

        Log.Console("<color=cyan>---- searching for \"" + command + "\" ----</color>");

        int results = 0;

        foreach (KeyValuePair<string, CommandInfo> kvp in cmnds)
        {
            if(kvp.Key.IndexOf(command) == 0 || kvp.Key.Contains(command))
            {
                Log.Console(kvp.Key);
                results++;
            }
        }

        if (results == 0)
            Log.Console("No results found.");
    }

    [ConsoleCommand("help", "Shows helpful information for a command")]
    public static void Help(string command)
    {
        if (ConsoleController.Get.Commands.ContainsKey(command))
        {
            CommandInfo info = ConsoleController.Get.Commands[command];

            Log.Console("<color=cyan>" + info.Name + " - " + info.Description + "</color>");
            Log.Console("usage: " + ConsoleController.Get.get_signature(command));
        }
        else
        {
            Log.Console("Invalid command '" + command + "'");
        }
    }

    public InputField CommandInput;

    public class CommandInfo
    {
        public string Name;
        public string Description;
        public MethodInfo Method;

        public CommandInfo(string name, string desc, MethodInfo method)
        {
            Method = method;
            Name = name;
            Description = desc;
        }
    }

    public string OldCommands;
    public Dictionary<string, CommandInfo> Commands = new Dictionary<string, CommandInfo>();
    public bool CommandConsoleOpen;

    [ConsoleCommand("test")]
    public static void Test()
    {
        Log.Console("Hello world!");
    }

    public static ConsoleController Get
    {
        get
        {
            if(_console == null)
            {
                _console = GameObject.FindObjectOfType<ConsoleController>();
            }

            return _console;
        }
    }

    static ConsoleController _console;

    List<string> _prevCommands;
    int _currentPrevCommand;

    protected override void OnControllerSetup()
    {
        _prevCommands = new List<string>();
        Assembly ass = Assembly.GetAssembly(typeof(ConsoleCommand));

        System.Type[] types = ass.GetTypes();

        foreach(System.Type t in types)
        {
            MethodInfo[] methods = t.GetMethods();
            foreach(MethodInfo method in methods)
            {
                object[] attrs = method.GetCustomAttributes(false);
                foreach(object attr in attrs)
                {
                    if(attr is ConsoleCommand)
                    {
                        ConsoleCommand cmd = (ConsoleCommand)attr;
                        MethodInfo info = method;
                        Commands.Add(cmd.Command, new CommandInfo(cmd.Command, cmd.Description, info));
                    }
                }
            }
        }

        base.OnControllerSetup();

        string gamestart_path = Application.dataPath + "/gamestart.txt";

        Log.Console(gamestart_path);

        if (System.IO.File.Exists(gamestart_path))
        {
            Log.Console("<color=cyan>Found gamestart file.</color>");
            string[] lines = System.IO.File.ReadAllLines(gamestart_path);

            for (int i = 0; i < lines.Length; ++i)
            {
                read_command(lines[i]);
            }
        }

    }

    string get_signature(string cmd)
    {
        ParameterInfo[] infos = Commands[cmd].Method.GetParameters();
        string signature = cmd + " ";
        foreach(ParameterInfo parameter in infos)
        {
            signature += "[" + parameter.Name + "] ";
        }
        signature = signature.TrimEnd(' ');

        return signature;
    }

    void read_command(string cmd)
    {
        _currentPrevCommand = 0;
        _prevCommands.Insert(0, cmd);

        string[] cmd_args = cmd.Split(' ');

        if (Commands.ContainsKey(cmd_args[0]))
        {
            string[] args = new string[cmd_args.Length - 1];

            for (int i = 0; i < args.Length; ++i)
            {
                args[i] = cmd_args[i + 1];
            }

            OldCommands += "\n" + cmd;
            try
            {
                if (args.Length != Commands[cmd_args[0]].Method.GetParameters().Length)
                {
                    Log.Console("<color=red>Invalid arguments. usage: " + get_signature(cmd_args[0]) + "</color>");
                }
                else
                {

                    if (args.Length > 0)
                        Commands[cmd_args[0]].Method.Invoke(null, args);
                    else
                        Commands[cmd_args[0]].Method.Invoke(null, null);
                }
            }
            catch (System.Exception e)
            {
                if (e.InnerException != null)
                {
                    Log.Console("<color=red>" + e.InnerException.GetType().Name + ": " + e.InnerException.Message + " " + e.InnerException.StackTrace + "</color>");
                    Debug.LogException(e.InnerException);
                }
                else
                {

                    Log.Console("<color=red>" + e.GetType().Name + ": " + e.Message + " " + e.StackTrace + "</color>");
                    Debug.LogException(e);
                }
            }
        }
        else
        {
            OldCommands += "\nCommand not found: " + cmd_args[0];
        }

        CommandInput.textComponent.text = "";
        CommandInput.text = "";
    }

    protected override void Update()
    {
        base.Update();

        if (Input.GetKeyDown(KeyCode.BackQuote))
        {
            CommandConsoleOpen = !CommandConsoleOpen;

            if (CommandConsoleOpen)
            {
                InputSystem.Get.PushFilter(InputSystem.FILTER_CONSOLE);
            }
            else
            {
                InputSystem.Get.PopFilter();
            }

        }

        if (CommandConsoleOpen)
        {
            if(CommandInput.text == "\n")
            {
                CommandInput.text = "";
            }

            if(_prevCommands.Count > 0 && Input.GetKeyDown(KeyCode.UpArrow))
            {
                _currentPrevCommand++;
                _currentPrevCommand %= _prevCommands.Count;

                CommandInput.text = _prevCommands[_currentPrevCommand];
            }

            if (!CommandInput.isFocused)
            {
                CommandInput.Select();
                CommandInput.ActivateInputField();
            }


            if (Input.GetKeyDown(KeyCode.Return))
            {

                string cmd = CommandInput.text;

                read_command(cmd);
                
            }
        }

    }
}