﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class Log 
{
    public static void Console(object message)
    {
        ConsoleController.Get.OldCommands += "\n" + message;
    }

    public static void Data(object message)
    {
        Debug.Log("[Data] : " + message);
    }
    public static void Systems(object message)
    {
        Debug.Log("[Systems] : " + message);
    }

    public static void DebugMsg(object message, Object context = null)
    {
        Debug.Log("debug : " + message, context);
        //ChatController.Get.AddMessage(message.ToString(), "debug", ChatController.ChatMessage.Type.Event);
    }

    public static System.Exception Assert(string condition, object message)
    {
        System.Exception e = new System.Exception("*ASSERT* (" + condition + ") : " + message);
        Debug.LogWarning(e.Message);

        return e;
    }

    public static void AssertIf(bool condition, object message)
    {
        if (condition)
        {
            throw new System.Exception("*ASSERT* : " + message);
        }

    }

    public static void Error(object message, Object context = null)
    {
        Debug.LogWarning("*ERROR* : " + message, context);
    }

    public static void Exception(System.Exception e)
    {
        Debug.LogWarning("*ERROR* : " + e.GetType().Name + " - " + e.Message + "\n" + e.StackTrace);
    }
}
