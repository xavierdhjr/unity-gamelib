﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ResourceCache 
{
    public static ResourceCache Get
    {
        get
        {
            if (_system == null)
                _system = new ResourceCache();
            return _system;
        }
    }

    static ResourceCache _system;

    private class CachedObject
    {
        public string Name;
        public Object obj;
    }

    List<CachedObject> _objs = new List<CachedObject>();

    public T GetObject<T>(string resourceName)
        where T: Object
    {
        for (int i = 0; i < _objs.Count; ++i)
        {
            if (_objs[i].Name == resourceName)
                return (T)_objs[i].obj;
        }


        if (LoadAndCache(resourceName, typeof(T)))
            return GetObject<T>(resourceName);
        else
            throw new System.Exception("Could not load object '" + resourceName + "'.");

    }

    bool LoadAndCache(string resourceName, System.Type type)
    {
        CachedObject cachedObj = new CachedObject();

        Object[] objs = Resources.LoadAll(resourceName, type);
        if (objs.Length <= 0)
            return false;

        Object o = objs[0];
        if (o == null || !(type.IsAssignableFrom(o.GetType())))
            return false;

        cachedObj.obj = o;
        cachedObj.Name = resourceName;
        _objs.Add(cachedObj);

        return true;
    }

}
