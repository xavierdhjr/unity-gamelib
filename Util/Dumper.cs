﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public interface IDumperino
{
	string GetInfoDump();
}

/// <summary>
/// Dumps debug info.
/// </summary>
public class Dumper
{
	string _title;
	System.Text.StringBuilder _strBlder;
	string _dump;
	
	public Dumper(string title)
	{
		_title = title;
		_strBlder = new System.Text.StringBuilder();
		_strBlder.AppendLine("#### " + _title + " ####");
	}
	
	public void AddDivider()
	{
		_strBlder.AppendLine("-----------------");
	}
	
	public void AddLine(string line)
	{
		_strBlder.AppendLine(line);
	}
	
	public void AddProp(string name, object value)
    {
        string val = "<color=red>NULL</color>";
        if (value != null)
        {
            if (value is IEnumerable)
            {
                val = value.GetType() + " { ";
                IEnumerable e = (IEnumerable)value;
                foreach (object o in e)
                {
                    if (o != null)
                    {
                        val += o.ToString();
                    }
                    else
                    {
                        val += "<color=red>NULL</color>";
                    }

                    val += ",";
                }
                val += " }";
            }
            else
            {
                val = value.ToString();
            }
        }
        _strBlder.AppendLine(name + ": " + val);
	}
	
	public void Add(string s)
	{
		_strBlder.Append(s);
	}
	
	public string GetDump()
	{
		if(string.IsNullOrEmpty(_dump))
		{
			_strBlder.AppendLine("--- end dump ---");
			_dump = _strBlder.ToString();
		}
		
		return _dump;
	}
}