﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Helper class that adds Time.deltaTime every frame until a time limit is reached. 
/// </summary>
public class FrameTimer
{
    float _currentTime;
    float _maxTime;

    public float Progress { get { return _currentTime / _maxTime; } }

    public FrameTimer(float seconds)
    {
        _maxTime = seconds;
        _currentTime = 0;
    }

    public bool NextFrame(float dt)
    {
        _currentTime += dt;

        if (_currentTime >= _maxTime)
        {
            _currentTime = _maxTime;
            return false;
        }

        return true;
    }

    public void Reset()
    {
        _currentTime = 0;
    }

}
