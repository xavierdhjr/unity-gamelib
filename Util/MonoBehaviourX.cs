﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public abstract class MonoBehaviourX : MonoBehaviour 
{
    public bool Destroyed
    {
        get { return _destroyed; }
    }
    public bool Error
    {
        get { return _error; }
    }

    bool _error;
    bool _destroyed;

    protected virtual void OnDestroy()
    {
        _destroyed = true;
    }

    protected void OnErrorLog(string message)
    {
        Debug.LogError("(Behaviour " + this.gameObject + ") **** ERROR: " + message);
        _error = true;
    }
    protected void OnWarningLog(string message)
    {
        Debug.LogWarning("(Behaviour " + this.gameObject + ") ** WARNING: " + message);
    }
    protected void OnLog(string message)
    {
        Debug.Log("(Behaviour " + this.gameObject + "): " + message);
    }
}
