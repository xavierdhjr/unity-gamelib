﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class DefaultPlayerInput : PlatformInput 
{

    public const int BTN_ROTATE_LEFT = 1;
    public const int BTN_ROTATE_RIGHT = 2;

    public const int BTN_MOVE_LEFT = 3;
    public const int BTN_MOVE_RIGHT = 4;
    public const int BTN_MOVE_DOWN = 5;
    public const int BTN_MOVE_UP = 6;

    public const int BTN_START = 7;

    public TimeRepeat Repeater;
    public TimeRepeat PieceDownRepeater;

    public DefaultPlayerInput()
    {
        Repeater = new TimeRepeat(0.1f, 0.1f);

        GameSettings.Platform[] platforms = new GameSettings.Platform[]{
            GameSettings.Platform.Windows,
            GameSettings.Platform.Mac
        };

        MapButton(BTN_ROTATE_LEFT, platforms, KeyCode.A);
        MapButton(BTN_ROTATE_RIGHT, platforms, KeyCode.S);
        MapButton(BTN_MOVE_LEFT, platforms, KeyCode.LeftArrow);
        MapButton(BTN_MOVE_RIGHT, platforms, KeyCode.RightArrow);
        MapButton(BTN_MOVE_UP, platforms, KeyCode.UpArrow);
        MapButton(BTN_MOVE_DOWN, platforms, KeyCode.DownArrow);

        MapButton(BTN_START, platforms, KeyCode.Return);
    }



}
