﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class XboxBtn
{
    public const int A      = 0;
    public const int X      = 1;
    public const int B      = 2;
    public const int Y      = 3;
    public const int RB     = 4;
    public const int LB     = 5;

    public const int UP     = 6;
    public const int RIGHT  = 7;
    public const int LEFT   = 8;
    public const int DOWN   = 9;

    public const int START = 50;
}

public class XboxControllerInput : PlatformInput 
{
    public const int BTN_GREEN = XboxBtn.A;
    public const int BTN_BLUE = XboxBtn.X;
    public const int BTN_RED = XboxBtn.B;
    public const int BTN_YELLOW = XboxBtn.Y;
    public const int BTN_PURPLE = XboxBtn.RB;
    public const int BTN_OTHER_PURPLE = XboxBtn.LB;
    public const int BTN_START = XboxBtn.START;


    public const int L_AXIS_VERTICAL = 60;
    public const int L_AXIS_HORIZONTAL = 70;
    public const int DPAD_AXIS_VERTICAL = 80;
    public const int DPAD_AXIS_HORIZONTAL = 90;

    public const int BTN_BACK = 100;

    public XboxControllerInput(int player)
    {
        /*
        KeyCode mac_green = PlatformInput.GetKey_ForJoystickButtonN(player, 16);
        KeyCode mac_blue = PlatformInput.GetKey_ForJoystickButtonN(player, 18);
        KeyCode mac_red = PlatformInput.GetKey_ForJoystickButtonN(player, 17);
        KeyCode mac_yellow = PlatformInput.GetKey_ForJoystickButtonN(player, 19);
        KeyCode mac_purple = PlatformInput.GetKey_ForJoystickButtonN(player, 14);

        KeyCode win_green = PlatformInput.GetKey_ForJoystickButtonN(player, 0);
        KeyCode win_blue = PlatformInput.GetKey_ForJoystickButtonN(player, 2);
        KeyCode win_red = PlatformInput.GetKey_ForJoystickButtonN(player, 1);
        KeyCode win_yellow = PlatformInput.GetKey_ForJoystickButtonN(player, 3);
        KeyCode win_purple = PlatformInput.GetKey_ForJoystickButtonN(player, 5);
        
        MapButton(BTN_GREEN, GameSettings.Platform.Mac, mac_green);
        MapButton(BTN_BLUE, GameSettings.Platform.Mac, mac_blue);
        MapButton(BTN_RED, GameSettings.Platform.Mac, mac_red);
        MapButton(BTN_YELLOW, GameSettings.Platform.Mac, mac_yellow);
        MapButton(BTN_PURPLE, GameSettings.Platform.Mac, mac_purple);
        */

        GameSettings.Platform[] windows_and_mac = new GameSettings.Platform[]{ 
            GameSettings.Platform.Windows, 
            GameSettings.Platform.Mac,
            GameSettings.Platform.IOS
        };

        for (int i = 0; i < windows_and_mac.Length; ++i)
        {
            MapAxis(BTN_GREEN, windows_and_mac[i], "A_" + player);
            MapAxis(BTN_RED, windows_and_mac[i], "B_" + player);
            MapAxis(BTN_BLUE, windows_and_mac[i], "X_" + player);
            MapAxis(BTN_YELLOW, windows_and_mac[i], "Y_" + player);
            MapAxis(BTN_PURPLE, windows_and_mac[i], "RB_" + player);
            MapAxis(BTN_OTHER_PURPLE, windows_and_mac[i], "LB_" + player);
            MapAxis(BTN_START, windows_and_mac[i], "Start_" + player);

            MapAxis(DPAD_AXIS_HORIZONTAL, windows_and_mac[i], "DPad_XAxis_" + player);
            MapAxis(DPAD_AXIS_VERTICAL, windows_and_mac[i], "DPad_YAxis_" + player);
            MapAxis(L_AXIS_HORIZONTAL, windows_and_mac[i], "L_XAxis_" + player);
            MapAxis(L_AXIS_VERTICAL, windows_and_mac[i], "L_YAxis_" + player);
        }

        if (player == 1)
        {
            MapButton(BTN_GREEN, windows_and_mac, KeyCode.Alpha1);
            MapButton(BTN_BLUE, windows_and_mac, KeyCode.Alpha2);
            MapButton(BTN_RED, windows_and_mac, KeyCode.Alpha3);
            MapButton(BTN_YELLOW, windows_and_mac, KeyCode.Alpha4);
            MapButton(BTN_PURPLE, windows_and_mac, KeyCode.Alpha5);
            MapButton(BTN_START, windows_and_mac, KeyCode.Return);

            MapButton(XboxBtn.UP, windows_and_mac, KeyCode.UpArrow);
            MapButton(XboxBtn.DOWN, windows_and_mac, KeyCode.DownArrow);
            MapButton(XboxBtn.LEFT, windows_and_mac, KeyCode.LeftArrow);
            MapButton(XboxBtn.RIGHT, windows_and_mac, KeyCode.RightArrow);

            MapButton(XboxBtn.LB, windows_and_mac, KeyCode.Alpha6);

            MapButton(BTN_BACK, windows_and_mac, KeyCode.Escape);
        }
        else if(player == 2)
        {

            MapButton(BTN_GREEN, windows_and_mac, KeyCode.Q);
            MapButton(BTN_BLUE, windows_and_mac, KeyCode.W);
            MapButton(BTN_RED, windows_and_mac, KeyCode.E);
            MapButton(BTN_YELLOW, windows_and_mac, KeyCode.R);
            MapButton(BTN_PURPLE, windows_and_mac, KeyCode.T);
        }

        /*
        MapButton(BTN_GREEN, GameSettings.Platform.Windows, win_green, KeyCode.Alpha1);
        MapButton(BTN_BLUE, GameSettings.Platform.Windows, win_blue, KeyCode.Alpha2);
        MapButton(BTN_RED, GameSettings.Platform.Windows, win_red, KeyCode.Alpha3);
        MapButton(BTN_YELLOW, GameSettings.Platform.Windows, win_yellow, KeyCode.Alpha4);
        MapButton(BTN_PURPLE, GameSettings.Platform.Windows, win_purple, KeyCode.Alpha5);*/
    }

    public bool GetXboxButton(int btnId)
    {
        if (InputSystem.Get.GetCurrentFilter() == InputSystem.FILTER_STANDARD)
        {
            if (btnId == BTN_BACK) return GetButton(btnId);

            if (btnId == XboxBtn.UP)
            {
                return GetAxis(L_AXIS_VERTICAL) > 0 || GetAxis(DPAD_AXIS_VERTICAL) > 0 || GetButton(btnId);
            }
            else if (btnId == XboxBtn.LEFT)
            {
                return GetAxis(L_AXIS_HORIZONTAL) < 0 || GetAxis(DPAD_AXIS_HORIZONTAL) < 0 || GetButton(btnId);
            }
            else if (btnId == XboxBtn.RIGHT)
            {
                return GetAxis(L_AXIS_HORIZONTAL) > 0 || GetAxis(DPAD_AXIS_HORIZONTAL) > 0 || GetButton(btnId);
            }
            else if (btnId == XboxBtn.DOWN)
            {
                return GetAxis(L_AXIS_VERTICAL) < 0 || GetAxis(DPAD_AXIS_VERTICAL) < 0 || GetButton(btnId);
            }

            return GetAxisAsButton(btnId) || GetButton(btnId);
        }
        return false;
    }

    public bool GetXboxButtonDown(int btnId)
    {
        if (InputSystem.Get.GetCurrentFilter() == InputSystem.FILTER_STANDARD)
        {
            if (btnId == BTN_BACK) return GetButtonDown(btnId);

            if(btnId == XboxBtn.UP)
            {
                return GetAxis(L_AXIS_VERTICAL) > 0 || GetAxis(DPAD_AXIS_VERTICAL) > 0 || GetButtonDown(btnId);
            }
            else if(btnId == XboxBtn.LEFT)
            {
                return GetAxis(L_AXIS_HORIZONTAL) < 0 || GetAxis(DPAD_AXIS_HORIZONTAL) < 0 || GetButtonDown(btnId);
            }
            else if (btnId == XboxBtn.RIGHT)
            {
                return GetAxis(L_AXIS_HORIZONTAL) > 0 || GetAxis(DPAD_AXIS_HORIZONTAL) > 0 || GetButtonDown(btnId);
            }
            else if (btnId == XboxBtn.DOWN)
            {
                return GetAxis(L_AXIS_VERTICAL) < 0 || GetAxis(DPAD_AXIS_VERTICAL) < 0 || GetButtonDown(btnId);
            }

            return GetAxisAsButton(btnId) || GetButtonDown(btnId);
        }
        return false;
    }

    public float GetXboxAxis(int axisId)
    {
        float axis_val = GetAxis(axisId);

        if(axis_val == 0)
        {
            if (axisId == L_AXIS_VERTICAL)
            {
                if (Input.GetKey(KeyCode.UpArrow))
                    return 1;
                if (Input.GetKey(KeyCode.DownArrow))
                    return -1;
            }else if(axisId == L_AXIS_HORIZONTAL)
            {
                if (Input.GetKey(KeyCode.RightArrow))
                    return 1;
                if (Input.GetKey(KeyCode.LeftArrow))
                    return -1;
            }

        }

        return -axis_val;
    }
}
