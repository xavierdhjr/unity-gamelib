﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class IPadInput : PlatformInput 
{

	private class IPadCursorInfo : CursorInfo
	{
		Vector2 _lastCursorPos = Vector2.zero;
		public override Vector2 CursorPosition ()
		{
			if(Input.touchCount > 0) 
				_lastCursorPos = Input.GetTouch(InputSystem.CURSOR_PRIMARY).position;

			return _lastCursorPos;
		}

		public override bool GetCursorDown (int cursorNo)
		{
			if(cursorNo >= Input.touchCount) return false;

			Touch t = Input.GetTouch(cursorNo);

			return t.phase == TouchPhase.Began;
		}

		public override bool GetCursor (int cursorNo)
		{
			if(cursorNo >= Input.touchCount) return false;
			
			Touch t = Input.GetTouch(cursorNo);
			
			return t.phase == TouchPhase.Moved || t.phase == TouchPhase.Stationary;
		}

		public override bool GetCursorUp (int cursorNo)
		{
			if(cursorNo >= Input.touchCount) return false;
			
			Touch t = Input.GetTouch(cursorNo);
			
			return t.phase == TouchPhase.Ended;
		}

	}

	public IPadInput() : base(new IPadCursorInfo())
	{

	}

	const float SCROLL_SPEED = 7f;

	public override float GetScroll ()
	{ 


		if(Input.touchCount == 2)
		{        

			Touch touchZero = Input.GetTouch(0);
			Touch touchOne = Input.GetTouch(1);

			// Find the position in the previous frame of each touch.
			Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
			Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
			
			// Find the magnitude of the vector (the distance) between the touches in each frame.
			float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
			float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
			
			// Find the difference in the distances between each frame.
			//float deltaMagnitudeDiff = Mathf.Clamp(prevTouchDeltaMag - touchDeltaMag, -2.5f, 2.5f);
			//Debug.Log(deltaMagnitudeDiff);
			//return -deltaMagnitudeDiff;

			float diff_percentage = touchDeltaMag / prevTouchDeltaMag;

			if(diff_percentage < 1)
			{
				return -SCROLL_SPEED * (1 - diff_percentage);
			}

			return SCROLL_SPEED * (diff_percentage - 1);
		}
		return 0;
	}

}
