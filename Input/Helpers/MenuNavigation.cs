﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class TimeRepeat
{
    float _timeUntilRepeat;
    float _delay;

    FrameTimer _repeatTimer;
    FrameTimer _delayTimer;

    bool _checked;

    public TimeRepeat(float timeUntilRepeat, float delay)
    {
        _timeUntilRepeat = timeUntilRepeat;
        _delay = delay;
        _repeatTimer = new FrameTimer(timeUntilRepeat);
        _delayTimer = new FrameTimer(delay);
    }

    public bool Update(float deltaTime, bool check_repeat)
    {

        if (check_repeat && !_checked)
        {
            _checked = true;
            return true;
        }
        else if (check_repeat && _checked)
        {
            if (!_repeatTimer.NextFrame(deltaTime))
            {
                if (!_delayTimer.NextFrame(deltaTime))
                {
                    _delayTimer = new FrameTimer(_delay);
                    return true;
                }
            }
        }
        else
        {
            _checked = false;
            _repeatTimer = new FrameTimer(_timeUntilRepeat);
            _delayTimer = new FrameTimer(_delay);
        }

        return false;
    }
}

/// <summary>
/// Generically navigating a menu and has support for detecting when
/// navigation is being held down and should repeat.
/// </summary>
public class MenuNavigation
{
    const float TIME_UNTIL_SCROLL_REPEAT = 0.2f;
    const float SCROLL_REPEAT_DELAY = 0.1f;

    int _selectedItem;
    FrameTimer _repeatTimer;
    FrameTimer _delayTimer;
    bool _navigated;

    public MenuNavigation()
    {

        _repeatTimer = new FrameTimer(TIME_UNTIL_SCROLL_REPEAT);
        _delayTimer = new FrameTimer(SCROLL_REPEAT_DELAY);
    }

    public void Reset()
    {
        _navigated = false;
        _selectedItem = 0;
    }

    public int UpdateNavigation(int itemCount, float vert, float deltaTime)
    {

        bool vert_is_down = vert != 0;

        if (vert_is_down && !_navigated)
        {
            _navigated = true;
            _selectedItem = navigate(vert, itemCount);
        }
        else if (vert_is_down && _navigated)
        {
            if (!_repeatTimer.NextFrame(deltaTime))
            {
                if (!_delayTimer.NextFrame(deltaTime))
                {
                    _delayTimer = new FrameTimer(SCROLL_REPEAT_DELAY);
                    navigate(vert, itemCount);
                }
            }
        }
        else
        {
            _navigated = false;
            _repeatTimer = new FrameTimer(TIME_UNTIL_SCROLL_REPEAT);
            _delayTimer = new FrameTimer(SCROLL_REPEAT_DELAY);
        }

        return _selectedItem;
    }


    int navigate(float direction, int itemCount)
    {
        if (direction < 0)
        {
            _selectedItem++;
            _selectedItem = mod(_selectedItem, itemCount);
        }
        else if (direction > 0)
        {
            _selectedItem--;
            _selectedItem = mod(_selectedItem, itemCount);
        }

        return _selectedItem;
    }

    // TIL C# modulo is not actually modulo, but remainder.
    int mod(int k, int n) { return ((k %= n) < 0) ? k + n : k; }
}
