﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public abstract class InputGesture
{
	public bool IsTrue
	{
		get{ return _isTrue; }
	}
	
	bool _isTrue;
	PlatformInput _input;
	
	protected PlatformInput _Input{ get{ return _input; } }
	protected int _CursorNo = InputSystem.CURSOR_PRIMARY;

	public abstract void UpdateDefinition(float dt);
	
	protected void SetDefinition(bool isTrue)
	{
		_isTrue = isTrue;
	}
	
	public static implicit operator bool(InputGesture gesture)
	{
		return gesture.IsTrue;
	}
	
	public void ListenForCursor(int cursorNo)
	{
		_CursorNo = cursorNo;
	}

	public void ReadFromInput(PlatformInput input)
	{
		_input = input;
	}
	
}

public struct Drag
{
	public Vector2 Start;
	public Vector2 End;
	public float Distance;
	public Vector2 Direction;
	
	public Drag(Vector2 start, Vector2 end)
	{
		Start = start;
		End = end;
		Direction = End - Start;
		Distance = Direction.magnitude;
	}
	
	public Drag ScreenToWorld(Camera toCamera)
	{
		return new Drag(toCamera.ScreenToWorldPoint(Start),toCamera.ScreenToWorldPoint(End));
	}
	
	public Drag WorldToScreen(Camera toCamera)
	{
		return new Drag(toCamera.WorldToScreenPoint(Start), toCamera.WorldToScreenPoint(End));
	}
	
}


public class InputGestureHold : InputGesture
{
    float _timePassed;
    float _holdThreshold;

    public InputGestureHold(float holdThreshold)
    {
        _holdThreshold = holdThreshold;
    }

    public override void UpdateDefinition(float dt)
    {
        SetDefinition(false);

        if (_Input.GetCursor())
        {
            if (_timePassed < _holdThreshold)
            {
                _timePassed += dt;
            }
            else
            {
                SetDefinition(true);
            }
        }
        else
        {
            _timePassed = 0;
        }
    }
}

public class InputGestureDoubleClick : InputGesture
{
	
	float _doubleClickDistanceThreshold;
	float _doubleClickInvalidTime;
	float _timePassed;
	
	bool _readyForDoubleClick;
	
	Vector2 _firstClick;
	Vector2 _secondClick;
	
	public InputGestureDoubleClick(float doubleClickTimeLimit, float distanceThreshold)
	{
		_doubleClickInvalidTime = doubleClickTimeLimit;
		_doubleClickDistanceThreshold = distanceThreshold;
	}

	public override void UpdateDefinition (float dt)
	{
		SetDefinition(false);
		
		if(!_readyForDoubleClick && _Input.GetCursorDown(_CursorNo))
		{
			_readyForDoubleClick = true;
			_firstClick = _Input.CursorPosition();
			return;
		}
		
		if(_readyForDoubleClick)
		{
			_timePassed += dt;
		}
		
		if(!_readyForDoubleClick)
			return;
		
		if(_timePassed > _doubleClickInvalidTime)
		{
			_timePassed = 0;
			_readyForDoubleClick = false;
		}
		else if(_Input.GetCursorDown(_CursorNo))
		{
			_secondClick = _Input.CursorPosition();
			if(Vector2.Distance(_firstClick, _secondClick) <= _doubleClickDistanceThreshold)
				SetDefinition(true);
		}
	}
	
}

public class InputGestureDrag : InputGesture
{
	
	float _minDragDistance;
	
	bool _dragging;
	
	Vector2 _startPos;
	Vector2 _lastEndPos;
	Vector2 _endPos;

	public InputGestureDrag(float minDragDistance)
	{
		_minDragDistance = minDragDistance;
	}
	
	public Drag GetScreenDrag()
	{
		
		return new Drag(_lastEndPos, _endPos);
	}

	public override void UpdateDefinition (float dt)
	{
		if(!_dragging)
		{
			SetDefinition(false);
		}
		else if(IsTrue && _Input.GetCursorUp())
		{
			_dragging = false;
			return;
		}
		
		_lastEndPos = _endPos;
		
		if(_Input.GetCursorDown(_CursorNo))
		{
			_startPos = _Input.CursorPosition();
		}

		if(_Input.GetCursor(_CursorNo))
		{
			float d = Vector2.Distance(_startPos, _endPos);
			if(d > _minDragDistance)
			{
				SetDefinition(true);
				_dragging = true;
			}
		}
		
		_endPos = _Input.CursorPosition();
	}


}


/// <summary>
/// Base class for mapping out input for an arbitrary button set
/// across any platform.
/// </summary>
public abstract class PlatformInput
{
	protected abstract class CursorInfo
	{
		public abstract Vector2 CursorPosition();
		public abstract bool GetCursorDown(int cursorNo);
		public abstract bool GetCursor(int cursorNo);
		public abstract bool GetCursorUp(int cursorNo);
	}

	private class DefaultCursorInfo : CursorInfo
	{
		public override Vector2 CursorPosition ()
		{
			Debug.Log("CURSOR POSITION : " + Input.mousePosition);
			return Input.mousePosition;
		}

		public override bool GetCursorDown (int cursorNo)
		{
			return Input.GetMouseButtonDown(cursorNo);
		}

		public override bool GetCursor (int cursorNo)
		{
			return Input.GetMouseButton(cursorNo);
		}

		public override bool GetCursorUp (int cursorNo)
		{
			return Input.GetMouseButtonUp(cursorNo);
		}
	}

	public const float DOUBLE_CLICK_TIME = 0.7f;
	public const float DOUBLE_CLICK_DISTANCE_THRESHOLD = 50;
	public const float DRAG_MIN_DISTANCE = 30;
    public const float HOLD_TIME = 0.5f;

	// Deprecated. Not generic enough.
	public enum Axis
	{
		LeftAnalog_Horizontal,
		LeftAnalog_Vertical,
		RightAnalog_Horizontal,
		RightAnalog_Vertical,
		RightTrigger,
		LeftTrigger
	}
	
	// Deprecated. Not generic enough.
	public enum Button
	{
		A,
		B,
		X,
		Y,
		Start,
		Select,
		RightBumper,
		LeftBumper
	}
	
	/// <summary>
	/// Returns a string representing the X axis for Player P
	/// </summary>
	public static string AxisX(int player)
	{
		return GetPrefix(player) + "_XAxis";
	}
	
	/// <summary>
	/// Returns a string representing the Y axis for Player P
	/// </summary>
	public static string AxisY(int player)
	{
		return GetPrefix(player) + "_YAxis";
	}
	
	/// <summary>
	/// Returns a string representing the Nth axis for Player P
	/// </summary>
	public static string AxisN(int player, int axisNo)
	{
		return GetPrefix(player) + "_" + FormatAxis(axisNo) + "Axis";
	}
	/// <summary>
	/// Returns a KeyCode representing the Nth Joystick Button for Player P
	/// </summary>
	public static KeyCode GetKey_ForJoystickButtonN(int playerNo, int joystickBtnNo)
	{
		string player = (playerNo > 0) ? playerNo.ToString() : "";
		KeyCode val = (KeyCode)System.Enum.Parse(typeof(KeyCode), "Joystick" + player + "Button" + joystickBtnNo);
		
		return val;
	}
	
	/// <summary>
	/// Returns a string representing the Nth Joystick Button for Player P
	/// </summary>
	public static string GetStr_ForJoystickButtonN(int player, int joystickBtnNo)
	{
		return GetPrefix(player) + "_JoystickButton" + joystickBtnNo;
	}
	
	static string GetPrefix(int player)
	{
		if (player > 0)
			return player.ToString();
		return "Any";
	}
	
	static string FormatAxis(int axis)
	{
		if (axis == 2)
			return "2nd";
		if (axis == 3)
			return "3rd";
		else
			return axis + "th";
	}
	
	// Dictionary of Dictionaries to contain the mappings for all platforms. 3Spooky5me. 
    Dictionary<GameSettings.Platform, Dictionary<int, List<KeyCode>>> _platformButtonMappings = new Dictionary<GameSettings.Platform, Dictionary<int, List<KeyCode>>>();
    Dictionary<GameSettings.Platform, Dictionary<int, List<string>>> _platformAxisMappings = new Dictionary<GameSettings.Platform, Dictionary<int, List<string>>>();

    Dictionary<int, List<KeyCode>> _selectedButtonMapping;
    Dictionary<int, List<string>> _selectedAxisMapping;
	
	InputGestureDrag _dragging = new InputGestureDrag(DRAG_MIN_DISTANCE);
	InputGestureDoubleClick _doubleClick = new InputGestureDoubleClick(DOUBLE_CLICK_TIME, DOUBLE_CLICK_DISTANCE_THRESHOLD);
    InputGestureHold _hold = new InputGestureHold(HOLD_TIME);

	CursorInfo _cursorInfo;

	string[] _platforms;
	
	public PlatformInput()
	{
		_cursorInfo = new DefaultCursorInfo();
		Init ();
	}

	void Init()
	{
		// Collect every value in the GameSettings.Platform enumeration.
		// Create dictionaries for all the different platform mappings.
		_platforms = System.Enum.GetNames(typeof(GameSettings.Platform));
		for (int i = 0; i < _platforms.Length; ++i)
		{
			GameSettings.Platform platform = GetPlatform(i);
			_platformButtonMappings.Add(platform, new Dictionary<int, List<KeyCode>>());
            _platformAxisMappings.Add(platform, new Dictionary<int, List<string>>());
		}

		SelectPlatform(GameSettings.CurrentPlatform);
		
		_dragging.ReadFromInput(this);
		_doubleClick.ReadFromInput(this);
        _hold.ReadFromInput(this);
	}

	protected PlatformInput(CursorInfo info)
	{
		_cursorInfo = info;
		Init ();
	}

	/// <summary>
	/// Type casts the enumeration value to an integer, which can then be used for mapping.
	/// Useful if you want to maintain an enumeration for mapping instead of constant integers.
	/// </summary>
	protected int ToID(System.Enum val)
	{
		return (int)(object)val;
	}
	
	/// <summary>
	/// platformId indexes into a private array of strings that can be parsed into 
	/// GameSettings.Platform enum values.
	/// </summary>
	protected GameSettings.Platform GetPlatform(int platformId)
	{
		return (GameSettings.Platform)System.Enum.Parse(typeof(GameSettings.Platform), _platforms[platformId]);
	}
	
	/// <summary>
	/// Maps an integer ID to a KeyCode for all platforms.
	/// </summary>
	protected void MapButtonForAllPlatforms(int btnId, KeyCode to_button)
	{
		for (int i = 0; i < _platforms.Length; ++i)
		{
			GameSettings.Platform platform = GetPlatform(i);
			MapButton(btnId, platform, to_button);
		}
	}
	
	[System.Obsolete("Use MapButtonForAllPlatforms instead.")]
	protected void MapKeyForAllPlatforms(Button button, KeyCode to_button)
	{
		MapButtonForAllPlatforms((int)button, to_button);
	}
	
	/// <summary>
	/// Maps an integer ID to a string representing an axis for all platforms.
	/// </summary>
	protected void MapAxisForAllPlatforms(int axisId, string to_axis)
	{
		for (int i = 0; i < _platforms.Length; ++i)
		{
			GameSettings.Platform platform = GetPlatform(i);
			MapAxis(axisId, platform, to_axis);
		}
	}
	
	[System.Obsolete("Use MapAxisForAllPlatforms(int, string) instead.")]
	protected void MapAxisForAllPlatforms(Axis axis, string to_axis)
	{
		MapAxisForAllPlatforms((int)axis, to_axis);
	}
	
	/// <summary>
	/// Maps an integer ID to a KeyCode for a given platform.
	/// </summary>
	protected void MapButton(int btnId, GameSettings.Platform platform, params KeyCode[] to_buttons)
	{
        if (!_platformButtonMappings[platform].ContainsKey(btnId))
        {
            _platformButtonMappings[platform].Add(btnId, new List<KeyCode>(to_buttons));
        }
        else
        {
            for (int i = 0; i < to_buttons.Length; ++i)
            {
                _platformButtonMappings[platform][btnId].Add(to_buttons[i]);
            }
        }
	}

    /// <summary>
    /// Maps an integer ID to a KeyCode for each of the given platforms
    /// </summary>
    protected void MapButton(int btnId, GameSettings.Platform[] platforms, params KeyCode[] to_buttons)
    {
        for (int i = 0; i < platforms.Length; ++i)
        {
            GameSettings.Platform platform = platforms[i];
            if (!_platformButtonMappings[platform].ContainsKey(btnId))
            {
                _platformButtonMappings[platform].Add(btnId, new List<KeyCode>(to_buttons));
            }
            else
            {
                for (int k = 0; k < to_buttons.Length; ++k)
                {
                    _platformButtonMappings[platform][btnId].Add(to_buttons[k]);
                }
            }
        }
    }
	
	/// <summary>
	/// Maps an integer ID to a string representing an axis for a given platform.
	/// </summary>
	protected void MapAxis(int axisId, GameSettings.Platform platform, params string[] to_axis)
	{
        if (!_platformAxisMappings[platform].ContainsKey(axisId))
        {
            for (int i = 0; i < to_axis.Length; ++i)
            {
                _platformAxisMappings[platform].Add(axisId, new List<string>(to_axis));
            }
        }
        else
        {
            for (int i = 0; i < to_axis.Length; ++i)
            {
                _platformAxisMappings[platform][axisId].Add(to_axis[i]);
            }
        }
	}
	
	/// <summary>
	/// Checks if the given ID is mapped to a button.
	/// </summary>
	protected bool IsBtnMapped(int btnId)
	{
		return _selectedButtonMapping.ContainsKey(btnId);
	}
	
	/// <summary>
	/// Checks if the given ID is mapped to an axis.
	/// </summary>
	protected bool IsAxisMapped(int axisId)
	{
		return _selectedAxisMapping.ContainsKey(axisId);
	}
	
	[System.Obsolete("Use MapButton instead.")]
	protected void MapKey(Button key, GameSettings.Platform platform, KeyCode to_button)
	{
		MapButton((int)key, platform, to_button);
	}
	
	[System.Obsolete("Use MapAxis(int, GameSettings.Platform, string) instead.")]
	protected void MapAxis(Axis axis, GameSettings.Platform platform, string to_axis)
	{
		MapAxis((int)axis, platform, to_axis);
	}
	
	[System.Obsolete("Use IsBtnMapped instead.")]
	bool IsMapped(Button button)
	{
		return _selectedButtonMapping.ContainsKey((int)button);
	}
	[System.Obsolete("Use IsAxisMapped instead.")]
	bool IsMapped(Axis axis)
	{
		return _selectedAxisMapping.ContainsKey((int)axis);
	}
	
	public void SelectPlatform(GameSettings.Platform platform)
	{
		_selectedButtonMapping = _platformButtonMappings[platform];
		_selectedAxisMapping = _platformAxisMappings[platform];
	}
	
	public void UpdateInput(float dt)
	{
		_dragging.UpdateDefinition(dt);
		_doubleClick.UpdateDefinition(dt);
        _hold.UpdateDefinition(dt);
	}
	
	public bool DoubleClick()
	{
		return _doubleClick;
	}
	
	public bool Dragging()
	{
		return _dragging;
	}
	
    public bool CursorHeldDown()
    {
        return _hold;
    }

	public Drag CursorDrag()
	{
		return _dragging.GetScreenDrag();
	}

	public Vector2 CursorPosition()
	{
		return Input.mousePosition;
	}

	public bool GetCursor(int cursorId = InputSystem.CURSOR_PRIMARY)
	{
		/*
		return Input.GetMouseButton(cursorId) || 
			(cursorId < Input.touchCount && 
			 (Input.GetTouch(cursorId).phase == TouchPhase.Stationary || Input.GetTouch(cursorId).phase == TouchPhase.Moved));
			 */
		return _cursorInfo.GetCursor(cursorId);
	}

	public bool GetCursorDown(int cursorId = InputSystem.CURSOR_PRIMARY)
	{

		/*return Input.GetMouseButtonDown(cursorId) || (cursorId < Input.touchCount && 
		                                              Input.GetTouch(cursorId).phase == TouchPhase.Began);*/
		
		return _cursorInfo.GetCursorDown(cursorId);
	}

	public bool GetCursorUp(int cursorId = InputSystem.CURSOR_PRIMARY)
	{
		/*
		return Input.GetMouseButtonUp(cursorId) || (cursorId < Input.touchCount && 
		                                            Input.GetTouch(cursorId).phase == TouchPhase.Ended);*/
		
		return _cursorInfo.GetCursorUp(cursorId);
	}

	/// <summary>
	/// Gets the associated KeyCode mapping for btnId and calls Input.GetKey
	/// </summary>
	public bool GetButton(int btnId)
	{
        List<KeyCode> keys = _selectedButtonMapping[btnId];
		for(int i = 0; i < keys.Count; ++i)
        {
            if(Input.GetKey(keys[i]))
            {
                return true;
            }
        }

        return false;
	}

    public bool GetAxisAsButton(int axisId)
    {
        List<string> axes = _selectedAxisMapping[axisId];
        for(int i = 0; i < axes.Count; ++i)
        {
            if(Input.GetButtonDown(axes[i]))
            {
                return true;
            }
        }
        return false;
    }
	
	/// <summary>
	/// Gets the associated KeyCode mapping for btnId and calls Input.GetKeyDown
	/// </summary>
	public bool GetButtonDown(int btnId)
    {
        if (!_selectedButtonMapping.ContainsKey(btnId))
            return false;

        List<KeyCode> keys = _selectedButtonMapping[btnId];
        for (int i = 0; i < keys.Count; ++i)
        {
            if (Input.GetKeyDown(keys[i]))
            {
                return true;
            }
        }
        return false;
	}
	
	/// <summary>
	/// Gets the associated KeyCode mapping for btnId and calls Input.GetKeyUp
	/// </summary>
	public bool GetButtonUp(int btnId)
    {
        List<KeyCode> keys = _selectedButtonMapping[btnId];
        for (int i = 0; i < keys.Count; ++i)
        {
            if (Input.GetKeyUp(keys[i]))
            {
                return true;
            }
        }
        return false;
	}
	
	/// <summary>
	/// Gets the associated string mapping for axisId and calls Input.GetAxis
	/// </summary>
	public float GetAxis(int axisId)
    {
        List<string> axes = _selectedAxisMapping[axisId];
        for (int i = 0; i < axes.Count; ++i)
        {
            float axis = Input.GetAxis(axes[i]);
            if (Mathf.Abs(axis) > 0)
            {
                return axis;
            }
        }
        return 0;
	}

	public virtual float GetScroll()
	{
		return Input.GetAxis("Mouse ScrollWheel");
	}

    public List<KeyCode> GetKeysMappedToButton(int btnId)
    {

        return _selectedButtonMapping[btnId];
    }

}

