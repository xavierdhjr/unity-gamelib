﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class WToggles : WModuleBase 
{
    [Wrangular.Attributes.Returns(typeof(int))]
    public string ToggleOptionExpression;
    public List<Toggle> Toggles;
    public int StartingToggle;

    int _selectedOption;

    protected override void Start()
    {
        base.Start();

        for (int i = 0; i < Toggles.Count; ++i)
        {
            Toggles[i].isOn = false;
        }
        Toggles[StartingToggle].isOn = true;
    }

    public override bool IsExpressionMultipleExpressions
    {
        get { return false; }
    }

    protected override string ExpressionString()
    {
        return "{" + ToggleOptionExpression + "}";
    }

    protected override void Apply(string propName, object val)
    {
        int option = (int)val;

        _selectedOption = option;

    }

    void LateUpdate()
    {

        for (int i = 0; i < Toggles.Count; ++i)
        {
            if (i == _selectedOption)
            {
                if (!Toggles[i].isOn)
                    Toggles[i].isOn = true;
            }
            else
            {
                if (Toggles[i].isOn)
                    Toggles[i].isOn = false;
            }
        }
    }
}
