﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System;
using System.Reflection.Emit;
using System.Linq.Expressions;

public class ScopeMethodInfo
{
    public MethodInfo Method;
    public WScope Scope;
}

public class ScopeFixedValue
{
	public string Name;
	public object Value;
	public System.Type Type;
}

public class ScopeFieldInfo
{

	public WScope Scope;
	public FieldInfo Field;
	Func<object, object> _getter;

	public object GetValue(object inst)
	{
		//if (_getter != null) return _getter(inst);

		//_getter = GetGetter(Field);

		if (Field != null) return Field.GetValue(inst);
		else return null;

	}
}

public class ScopePropertyInfo
{
	public WScope Scope;
	public PropertyInfo Property;

	public object GetValue(object inst, object[] args)
	{
		if (Property == null) return null;

		return Property.GetValue(inst,args);
	}
}

[Serializable]
public class WBindingSettings
{
	public delegate void OnApply(string name, object value);
	/// <summary>
	/// If this function returns true, this binding will be updated on the next FixedUpdate()
	/// </summary>
	public delegate void OnPostApply();
	public delegate void OnBindSuccess(Dictionary<string, object> vals);

	public OnApply Apply;
	public OnPostApply PostApply;
	public OnBindSuccess BindSuccess;

	/// <summary>
	/// If this is true, the expression for this binding will be evaluated
	/// </summary>
	public bool ShouldEvaluate = true;

	/// <summary>
	/// If this is true, the binding will be apply()'d every FixedUpdate instead of
	/// whenever WScope has scheduled it.
	/// </summary>
	public bool ForceApply = false;
}

public class WBinding
{
	public bool BadBinding;
	public bool ForceApply { get { return _settings.ForceApply; } }
	public object Binder;
	public WExpression Expression;
	public string Name { get { return _name; } }
	public int NameHash{ get{ return _nameHash; } }
	public WScope ScopeBoundTo;
	public WBindingSettings Settings { get { return _settings; } }

	string _name;
	int _nameHash;
	object _cachedValue;
	WBindingSettings _settings;

	public static int Hash(string name)
	{
		int nameHash = 0;
		
		for(int i = 0; i < name.Length; ++i)
			nameHash += (int)name[i];

		return nameHash;
	}

	public WBinding(object binder, string name, WExpression expr, WBindingSettings settings)
	{
		_name = name;
		_settings = settings;
		Binder = binder;
		Expression = expr;

		_nameHash = Hash (name);
	}

	/// <summary>
	/// Returns whether this apply() function should be called in the next update.
	/// </summary>
	public void Apply()
	{
		if (_settings.ShouldEvaluate)
		{
			_cachedValue = Expression.Evaluate();
		}

		if(ScopeBoundTo.Hijacker != null)
			throw new System.Exception("Calling Apply() from hijacked scope " + ScopeBoundTo);

		_settings.Apply(_name, _cachedValue);

	}

	public void ManualApply(object value)
	{
		if (!_settings.ShouldEvaluate) return;

		_settings.Apply(_name, value);
	}

	public void PostApply()
	{
		if (_settings.PostApply != null)
			_settings.PostApply();
	}

}

public class WToken
{
	public static string[] TokenTypes = new string[]
        {
            WHITESPACE, STR_LITERAL, BOOL_LITERAL, LIST_OPERATOR, ID, INT_LITERAL, OPERATOR, UNKNOWN
        };

	public const string LIST_OPERATOR = @"^[(),\[\]]";
	public const string ID = @"^[a-zA-Z_]+[a-zA-Z_0-9]*";
	public const string INT_LITERAL = @"^[0-9]+";

	// Not really supported!
	public const string STR_LITERAL = @"""((\\.)|[^""])*""";
    public const string BOOL_LITERAL = @"^(true|false)";

	public const string OPERATOR = @"^\+|\-|\*|\%|&&|\|\||\.|/|>=|<=|==|!=|\>|\<|\[|\]"; // + - * % && ||
	public const string WHITESPACE = @"^[ \t]";
	public const string UNKNOWN = @"^.";

	public string Type;
	public string TypeName;
	public string Value;

	public override string ToString()
	{
		return TypeName + ": " + Value;
	}

	public static string GetName(string type)
	{
		switch (type)
		{
			case ID:
				return "ID";
			case INT_LITERAL:
				return "INT_LITERAL";
			case OPERATOR:
				return "OPERATOR";
			case LIST_OPERATOR:
				return "LIST_OPERATOR";
			case STR_LITERAL:
				return "STR_LITERAL";
            case BOOL_LITERAL:
                return "BOOL_LITERAL";
		}
		return "UNKNOWN";
	}

	public static string GetType(string name)
	{
		switch (name)
		{
			case "ID":
				return ID;
			case "INT_LITERAL":
				return INT_LITERAL;
			case "OPERATOR":
				return OPERATOR;
			case "LIST_OPERATOR":
				return LIST_OPERATOR;
			case "STR_LITERAL":
				return STR_LITERAL;
            case "BOOL_LITERAL":
                return BOOL_LITERAL;
		}
		return ".";
	}
}


public abstract class WLexer
{
	
	public abstract List<WToken> Lex(string text);
	
	
	public static WLexer Default { get { return new WDefaultLexer(); } }
}

public class WDefaultLexer : WLexer
{
	public override List<WToken> Lex(string expr)
	{
		List<WToken> tokens = new List<WToken>();
		
		if (expr == null)
			return tokens;
		
		while (expr.Length > 0)
		{
			int initial_length = expr.Length;
			
			foreach (string tokenType in WToken.TokenTypes)
			{
				Regex r = new Regex(tokenType);
				Match m = r.Match(expr);
				
				if (m.Success)
				{
					if (tokenType == WToken.UNKNOWN)
						throw new System.Exception("Unknown token '" + m.Value + "'");
					
					expr = expr.Substring(m.Value.Length);
					
					if (tokenType != WToken.WHITESPACE)
					{
						WToken t = new WToken()
						{
							Type = tokenType,
							TypeName = WToken.GetName(tokenType),
							Value = m.Value
						};
						tokens.Add(t);
					}
					break;
				}
			}
			
			if (initial_length == expr.Length)
			{
				foreach (WToken t in tokens)
					Debug.Log(t);
				throw new System.Exception("Internal error 1.");
			}
		}
		
		/*
			if(tokens.Count == 0)
			{

				//throw new System.Exception("No tokens generated from '" + original_expr + "'");
			}*/
		
		return tokens;
	}
	
}

public abstract class WScope : MonoBehaviourX 
{


	public bool Ready { get { return _ready; } }

	public WApp App { get { return _app; } }
	public WScope ParentScope { get { return _parent; } }
	public WScope Hijacker { get { return _hijacker; } }
	public int BindingCount { get { return _bindings.Count; } }
	public long ExecutionTicks { get { return _executionTime; } }
	public long AverageExecutionTime { get { return _avgExecutionTime; } }

	public int FPS{
		get{
			return (int)(1 / Time.deltaTime);
		}
	}

    public bool ApplyAllEveryUpdate;

	long _avgExecutionTime;
	int _samples;
	bool _ready;
	bool _hasCompiledScope;
	long _executionTime;

	WApp _app;
	WScope _parent;
	WScope _hijacker;

	Dictionary<string, ScopeFixedValue> _fixedProps = new Dictionary<string, ScopeFixedValue>();
	Dictionary<string, ScopeMethodInfo> _methods = new Dictionary<string, ScopeMethodInfo>();
	Dictionary<string, ScopeFieldInfo> _fields = new Dictionary<string, ScopeFieldInfo>();
	Dictionary<string, ScopePropertyInfo> _properties = new Dictionary<string, ScopePropertyInfo>();

	List<WBinding> _bindings = new List<WBinding>();

	public void Bind(object binder, string text, WBindingSettings settings)
	{
		AddJob(BindJob(binder, text, settings, this));
	}

	public void BindFrom(object binder, string text, WBindingSettings settings, WScope from)
	{
		
		AddJob(BindJob(binder, text, settings, from));
	}

	/// <summary>
	/// Hijacking a scope causes it to forward its events and all of its
	/// children's events to the scope specified.
	/// 
	/// Binding's have a property that specifies which scope is responsible
	/// for applying it.
    /// 
    /// THIS SUCKS RIGHT NOW... IDK WHY
	/// </summary>
	public void GetHijackedBy(WScope other)
	{
		_hijacker = other;
		/*
		if (!_ready)
		{
			OnScopeRegistered(other.App, other);
		}*/

		WScope[] children = transform.TraverseDownFor<WScope>(true); 
		foreach (WScope c in children)
		{
			if (c == this) continue;
			if (c == other) continue;
			/*
			if (!c.Ready)
				c.OnScopeRegistered(other.App, this);
			*/
			c.GetHijackedBy(other);
		}
	}

	public static List<string> ParseExpressions(string text)
	{
		List<string> exprs = new List<string>();
		string expr_match = @"\{[^{}]+\}"; // match any character except '{' or '}'

		Regex r = new Regex(expr_match);
		Match m = r.Match(text);

		do
		{
			string expr = m.Value.Trim('{', '}');
			exprs.Add(expr);

			text = r.Replace(text, "", 1);
			m = r.Match(text);
		} while (m != null && m.Success);

		return exprs;
	}

	IEnumerator BindJob(object binder, string text, WBindingSettings settings, WScope from = null)
	{
		while (!Ready)
			yield return null;

		if(_hijacker != null)
		{
			_hijacker.BindFrom(binder, text, settings, this);
			yield break;
		}


		Dictionary<string, object> vals = new Dictionary<string, object>();

		if(from == null)
			from = this;

		List<string> exprs = ParseExpressions(text);
        
        if(exprs.Count <= 0)
        {
            OnLog("Empty expression bound to " + gameObject + " from " + binder + ".");
            yield break;
        }

		foreach(string parsed_expr in exprs)
		{
			WBinding binding = new WBinding(binder, parsed_expr, Generate(parsed_expr), settings);
			binding.ScopeBoundTo = from;
			vals.Add(parsed_expr, "");
			_bindings.Add(binding);
		}

		if(settings.BindSuccess != null)
			settings.BindSuccess(vals);
		yield return null;
	}


	void OnDisable()
	{
		if(!_ready)
		{
			if(WApp.Get != null)
				WApp.Get.RegisterScope(this);
		}
	}

	public WExpression Generate(string expression)
	{
		List<WToken> tokens = WLexer.Default.Lex(expression);
		WExpression compiledExpression = new WExpression(this, tokens);
		return compiledExpression;
	}

	public WScope GetScopeForExpression(string expression)
	{
		return this;
	}

	public virtual void OnScopePropertySetup(WApp app, WScope parent)
	{
		if (parent != null && parent != this)
		{
			_parent = parent;

			if (_parent.ParentScope == null)
			{
				_parent.OnScopePropertySetup(app, _parent.transform.TraverseHierarchyFor<WScope>(true));
			}
			else
			{
				_parent.OnScopePropertySetup(app, _parent.ParentScope);
			}
			
			foreach (ScopeMethodInfo parentMethod in _parent.GetMethods())
			{
				AddMethod(parentMethod.Method, parentMethod.Scope);
			}
		}
		
		System.Type type = GetType();
        AddScopeInfoFromType(type);

	}

    protected void AddScopeInfoFromType(System.Type type)
    {
        foreach (MethodInfo method in type.GetMethods())
        {
            AddMethod(method, this);
        }

        foreach (FieldInfo field in type.GetFields())
        {
            AddField(field, this);
        }

        foreach (PropertyInfo prop in type.GetProperties())
        {
            AddProperty(prop, this);
        }
    }

	public void OnScopeRegistered(WApp app, WScope parent)
	{
		if (Ready)
		{
			return;
			// Maybe should throw this error to enforce ordering..
			//throw new System.Exception("Trying to register this scope (" + gameObject + ") more than once!");
		}

		_app = app;

		if (parent != null)
		{
			// This needs to happen for complicated ordering reasons
			WScope parentParent = parent.ParentScope;
			if(parent.ParentScope == null)
			{
				// Must exclude self from the search
				parentParent = parent.transform.TraverseHierarchyFor<WScope>(true);
			}

			parent.OnScopeRegistered(app, parentParent);
		}

		OnScopePropertySetup(app, parent);

		_ready = true;
		OnScopeReady();

	}

	public IEnumerable<ScopeFixedValue> GetFixedProperties()
	{
		return _fixedProps.Values;
	}

	public IEnumerable<ScopeMethodInfo> GetMethods()
	{
		return _methods.Values;
	}

	public IEnumerable<ScopePropertyInfo> GetProperties()
	{
		return _properties.Values;
	}

	public IEnumerable<ScopeFieldInfo> GetFields()
	{
		return _fields.Values;
	}

	public bool HasFixedProp(string prop)
	{
		if (_fixedProps.ContainsKey(prop))
			return true;
		if (ParentScope != null)
		{
			return ParentScope.HasFixedProp(prop);
		}
		return false;
	}

	public object GetFixedProp(string name)
	{
		ScopeFixedValue fixedVal;
		if(!_fixedProps.TryGetValue(name, out fixedVal))
		{
			if (ParentScope != null) return ParentScope.GetFixedProp(name);
			return null;
		}
		return fixedVal.Value;
	}

	public ScopeMethodInfo GetMethod(string name)
	{
		if (!_methods.ContainsKey(name))
		{
			if (ParentScope != null) return ParentScope.GetMethod(name);
			return null;
		}
		return _methods[name];
	}
	public ScopeFieldInfo GetField(string name)
	{
		if (!_fields.ContainsKey(name))
		{
			if (ParentScope != null) return ParentScope.GetField(name);
			return null;
		}
		return _fields[name];
	}
	public ScopePropertyInfo GetProperty(string name)
	{
		if (!_properties.ContainsKey(name))
		{
			if (ParentScope != null) return ParentScope.GetProperty(name);
			return null;
		}
		return _properties[name];
	}

	public void ClearProps()
	{
		_fixedProps.Clear();
	}

	protected abstract void OnScopeReady();

	/// <summary>
	/// Called by compiled classes to free up memory and stuff
	/// </summary>
	protected void ClearAllSettings()
	{
		_fixedProps.Clear();
		_methods.Clear();
		_fields.Clear();
		_properties.Clear();
		_bindings.Clear();
	}

	protected void SetReady(bool ready)
	{
		_ready = ready;

	}


	protected void AddFixedProp(string prop, object val, System.Type type = null)
	{
		if(_fixedProps.ContainsKey(prop))
		{
			_fixedProps[prop].Value = val;
			return;
		}

		System.Type propType = null;

		if (type != null)
			propType = type;
		else if (val != null)
			propType = val.GetType();
		else
			propType = typeof(object);


		_fixedProps.Add(prop, new ScopeFixedValue()
		{
			Name = prop,
			Value = val,
			Type = propType
		});
	}

	protected void AddMethod(MethodInfo m, WScope scope)
	{
		if (_methods.ContainsKey(m.Name))
		{
			_methods[m.Name].Method = m;
			_methods[m.Name].Scope = scope;
			return;
		}

		_methods.Add(m.Name, new ScopeMethodInfo()
			{
				Scope = scope,
				Method = m
			});
	}

	protected void AddField(FieldInfo f, WScope scope)
	{
		if (_fields.ContainsKey(f.Name))
		{
			_fields[f.Name].Field = f;
			_fields[f.Name].Scope = scope;
			return;
		}
		_fields.Add(f.Name, new ScopeFieldInfo()
			{
				Scope = scope,
				Field = f
			});
	}

	protected void AddProperty(PropertyInfo p, WScope scope)
	{
		if (_properties.ContainsKey(p.Name))
		{
			_properties[p.Name].Property = p;
			_properties[p.Name].Scope = scope;
			return;
		}
		_properties.Add(p.Name, new ScopePropertyInfo()
		{
			Scope = scope,
			Property = p
		});
	}

	protected void AddJob(IEnumerator job)
	{
		_jobs.Add(job);
	}

	List<IEnumerator> _jobs = new List<IEnumerator>();
	int _nextBindingToRun = 0;
	const int MAX_BINDINGS_PER_UPDATE = 20;
	const int RUN_EVERY_FIXED_UPDATES = 1;

	int _fixedUpdateCount;

    /// <summary>
    /// Applys all bindings immediately AND runs their post applys
    /// </summary>
    protected void ApplyAllBindings()
    {
        for(int i = 0; i < _bindings.Count; ++i)
        {
            WBinding binding = _bindings[i];
            if (binding.BadBinding) continue; 
            try
            {
                OnApplyBinding(_bindings[i]);
            }
            catch (System.Exception e)
            {
                binding.BadBinding = true;

                Debug.LogError("Failed to apply binding in " + this + " on " + binding.Binder);
                binding.Expression.Dump();
                Debug.LogException(e, (UnityEngine.Object)binding.Binder);
            }
        }

        for (int i = 0; i < _bindings.Count; ++i)
        {
            WBinding binding = _bindings[i];
            if (binding.BadBinding) continue;
            try
            {
                _bindings[i].PostApply();
            }
            catch (System.Exception e)
            {
                binding.BadBinding = true;

                Debug.LogError("Failed to post-apply binding in " + this + " on " + binding.Binder);
                Debug.LogException(e, (UnityEngine.Object)binding.Binder);
            }
        }
    }

    /// <summary>
    /// Run all jobs right meow
    /// </summary>
    protected void RunJobsImmediately()
    {
        for (int i = 0; i < _jobs.Count; ++i)
        {
            while (_jobs[i].MoveNext())
            {
            }
            _jobs.RemoveAt(i);
            i--;
        }
    }

	protected void OnFixedUpdate()
	{
		for (int i = 0; i < _jobs.Count; ++i)
		{
			if (!_jobs[i].MoveNext())
			{
				_jobs.RemoveAt(i);
				i--;
			}
		}

		_fixedUpdateCount++;
		if(_fixedUpdateCount < RUN_EVERY_FIXED_UPDATES)
		{
			return;
		}
		_fixedUpdateCount = 0;

		if (_hijacker != null)
			return;

		// Don't run any code if we have no bindings.
		if (_bindings.Count > 0)
		{
			
			// Distribute Apply() calls 
			int maxBinding = Mathf.Min(_nextBindingToRun + MAX_BINDINGS_PER_UPDATE, _bindings.Count);

			// If we have a hijacker, just run every call every frame
			if(_hijacker != null)
				maxBinding = _bindings.Count;

			for (int i = _nextBindingToRun; i < maxBinding; i++)
			{
				WBinding binding = _bindings[i];
				// Binding has encountered an error?
				if (binding.BadBinding) continue;
				try
				{
					// Up to the binding whether to evaluate.
					//if (_hijacker != null)
					//	_hijacker.OnApplyBinding(_bindings[i]);
					//else
					OnApplyBinding(binding);
				}
				catch (System.Exception e)
				{
					binding.BadBinding = true;

                    Debug.LogError("Failed to apply binding in " + this + " on " + binding.Binder, (UnityEngine.Object)binding.Binder);
					binding.Expression.Dump();
					Debug.LogException(e, (UnityEngine.Object)binding.Binder);
				}
				
			}
			
			// If the binding wasn't applied, but it should have been, apply it.
			for (int i = 0; i < _bindings.Count; ++i)
			{
				// Skip the bindings we just ran - dont run them twice!
				if(i >= _nextBindingToRun && i <= maxBinding)
				{
					continue;
				}
				
				if (_bindings[i].ForceApply)
				{
					//if (_hijacker != null)
					//	_hijacker.OnApplyBinding(_bindings[i]);
					//else
					OnApplyBinding(_bindings[i]);
				}

			}

			
			for (int i = 0; i < _bindings.Count; ++i)
			{
				// Run post applys on bindings we just ran
				if(i >= _nextBindingToRun && i <= maxBinding)
				{
					_bindings[i].PostApply();
				}
			}
			
			// start over if we've hit _bindings.Count
			_nextBindingToRun = maxBinding % _bindings.Count;

			/*
			// Trigger any post apply events
			foreach (WBinding binding in _bindings)
			{
				binding.PostApply();
			}*/
		}
		else
		{
			// Start off at 0 - we have no bindings at the moment
			_nextBindingToRun = 0;
		}

	}

	public void ApplyBinding(WBinding bindingToApply)
	{
		OnApplyBinding(bindingToApply);
	}

	protected virtual void OnApplyBinding(WBinding bindingToApply)
	{
		if (_hijacker != null)
		{
			_hijacker.OnApplyBinding(bindingToApply);
		}
		else
		{
			bindingToApply.Apply();
		}

	}

	protected void ApplyParentBinding(WBinding bindingToApply)
	{
		/*
		if (ParentScope != null)
		{
			ParentScope.OnApplyBinding(bindingToApply);
		}
		*/
	}

	public bool IsReadyIncludedParents()
	{
		if(ParentScope != null)
		{
			return Ready && ParentScope.IsReadyIncludedParents();
		}

		return Ready;
	}

	protected virtual void FixedUpdate()
	{
		if (!IsReadyIncludedParents())
		{
			return;
		}
#if UNITY_EDITOR
		System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
		sw.Start();
#endif

        if (ApplyAllEveryUpdate)
        {
            RunJobsImmediately();
            ApplyAllBindings();
        }
        else
        {
            OnFixedUpdate();
        }

#if UNITY_EDITOR
		sw.Stop();
		_executionTime = sw.ElapsedTicks;
		_avgExecutionTime *= _samples;
		_samples++;
		_avgExecutionTime += _executionTime;
		_avgExecutionTime /= _samples;
#endif
	}

	public override string ToString ()
	{
        if (this == null)
            return "DESTROYED";
		return gameObject.name + " (" + this.GetType() + ")";
	}

	public string DumpProperties()
	{
		System.Text.StringBuilder blder = new System.Text.StringBuilder();

		blder.AppendLine("Fixed Props");
		blder.AppendLine("================");
		foreach(KeyValuePair<string, ScopeFixedValue> val in _fixedProps)
		{
			blder.AppendLine(val.Key + ": " + val.Value.Value);
		}
		if(_fixedProps.Count <= 0)
			blder.AppendLine("** None **");
		blder.AppendLine("----------------");

		
		blder.AppendLine("Methods");
		blder.AppendLine("================");
		foreach(KeyValuePair<string, ScopeMethodInfo> val in _methods)
		{
			blder.AppendLine(val.Key + ": " + val.Value.Method.Name);
		}
		if(_methods.Count <= 0)
			blder.AppendLine("** None **");
		blder.AppendLine("----------------");

		
		blder.AppendLine("Fields");
		blder.AppendLine("================");
		foreach(KeyValuePair<string, ScopeFieldInfo> val in _fields)
		{
			blder.AppendLine(val.Key + ": " + val.Value.Field.Name);
		}
		if(_methods.Count <= 0)
			blder.AppendLine("** None **");
		blder.AppendLine("----------------");

		blder.AppendLine("Properties");
		blder.AppendLine("================");
		foreach(KeyValuePair<string, ScopePropertyInfo> val in _properties)
		{
			blder.AppendLine(val.Key + ": " + val.Value.Property.Name);
		}
		if(_methods.Count <= 0)
			blder.AppendLine("** None **");
		blder.AppendLine("----------------");

		return blder.ToString();
	}
}
