﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class WRepeaterChild : BaseController 
{
	public class WChildBinding : WBinding
	{
		public WScope ChildScope;

		public WChildBinding(object binder, string name, WExpression expr, WBindingSettings settings)
			: base(binder, name, expr, settings)
		{
		}

	}

	public object RepeaterItem
	{
		get{
			return GetFixedProp(_fixedPropName);
		}
	}

	public object CachedRepeaterItem
	{
		get { return _cachedFixedItem; }
	}

	public int Index;
	public bool ReadyToDestroy;
	public bool SetReadyToGameUI;

	string _fixedPropName;
	object _cachedFixedItem;
	GameUI _ui;
	bool hasUi;

	protected void Awake ()
	{

		_ui = GetComponent<GameUI>();
		hasUi = _ui != null;
	}

	public override void OnScopePropertySetup(WApp app, WScope parent)
	{
		base.OnScopePropertySetup(app, parent);
	}

	public void ApplyChild(string prop, object val, int index)
	{
		//ClearProps();
		AddFixedProp(prop, val);
		_fixedPropName = prop;
		_cachedFixedItem = val;
		//Item = val;
		Index = index;
		//name = val.ToString();
	}

    public void UpdateChild()
    {
        RunJobsImmediately();
        ApplyAllBindings();
    }

	protected override void OnApplyBinding(WBinding bindingToApply)
	{
		if (ParentScope.Hijacker != null)
		{
			WChildBinding childBinding = new WChildBinding(
				bindingToApply.Binder,
				bindingToApply.Name,
				bindingToApply.Expression,
				bindingToApply.Settings);

			childBinding.ChildScope = this;
			childBinding.ScopeBoundTo = ParentScope;
			ParentScope.ApplyBinding(childBinding);
		}
		else
		{
            base.OnApplyBinding(bindingToApply);
		}
	}

	public void SetDestroyFlag(bool flag)
	{
		ReadyToDestroy = flag;
	}

	protected override void Update()
	{
		if (SetReadyToGameUI && hasUi)
			SetReady(ParentScope.Ready && _ui.Shown);
		else
			SetReady(ParentScope.Ready);

	}
}
