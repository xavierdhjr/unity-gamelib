﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class WSlider : WModuleBase 
{
	
	public override bool IsExpressionMultipleExpressions {
		get {
			return false;
		}
	}

	public string Value;
	
	protected override void Apply(string propName, object val)
	{
		GetComponent<Slider>().value = (float)val;
	}

	protected override string ExpressionString()
	{
		return "{" + Value + "}";
	}
}
