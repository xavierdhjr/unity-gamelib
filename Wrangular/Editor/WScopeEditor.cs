using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;

[CustomEditor(typeof(WScope),true)]
public class WScopeEditor : Editor 
{
	WScope _parent;

	void OnEnable()
	{
		/*
		WScope scope = target as WScope;
		if (scope.ParentScope == null)
			_parent = scope.transform.TraverseHierarchyFor<WScope>(true);
		else
			_parent = scope.ParentScope;*/
	}

	public override void OnInspectorGUI ()
	{
		WScope scope = target as WScope;
		if (!(scope is CompiledControllerBase))
		{
			if (scope.ParentScope != null)
			{
                if(Application.HasProLicense())
				    GUI.skin.box.normal.textColor = Color.white;
                GUILayout.Box("Parent: " + scope.ParentScope, GUILayout.ExpandWidth(true));
                if (Application.HasProLicense())
				    GUI.color = Color.white;
			}
		}
		if (Application.isPlaying)
		{
			if(!scope.IsReadyIncludedParents())
			{
				GUI.color = Color.red;

                if (Application.HasProLicense())
				    GUI.skin.box.normal.textColor = Color.white;
				GUILayout.Box("Not ready", GUILayout.ExpandWidth(true));

                if (Application.HasProLicense())
				    GUI.color = Color.white;
			}

			if (scope.Hijacker != null)
			{
				GUI.color = Color.yellow;
				GUI.skin.box.normal.textColor = Color.white;
				GUILayout.Box("Hijacked by " + scope.Hijacker, GUILayout.ExpandWidth(true));

                if (Application.HasProLicense())
				    GUI.color = Color.white;
			}

			EditorGUILayout.LabelField("Bindings", scope.BindingCount + "");
			EditorGUILayout.LabelField("Execution Time", scope.AverageExecutionTime + " ticks");
		}

		base.OnInspectorGUI ();

        /*
		if (!(scope is CompiledControllerBase))
		{
			if (GUILayout.Button("Compile"))
			{
				WCompiler c = new WCompiler();
				c.Compile(scope, "Compiled" + scope.GetType().Name);
			}
		}*/

	}

	void Update()
	{
		Repaint();
	}
}
