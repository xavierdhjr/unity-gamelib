﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using Wrangular.Attributes;
using Wrangular.Compiler;
using System.Reflection;


[CustomEditor(typeof(WModuleBase), true)]
[CanEditMultipleObjects]
public class WModuleBaseEditor : Editor 
{
    enum VarType
    {
        Method,
        Field,
        Property,
        Fixed
    }

    private class MatchingVarInfo
    {
        public string Name;
        public System.Type ReturnType;
        public System.Type[] Args;
        public string[] ArgNames;
        public VarType Type;

        public MatchingVarInfo(FieldInfo info)
        {
            Name = info.Name;
            ReturnType = info.FieldType;
            Type = VarType.Field;
        }

        public MatchingVarInfo(PropertyInfo info)
        {
            Name = info.Name;
            ReturnType = info.PropertyType;
            Type = VarType.Property;
        }

        public MatchingVarInfo(MethodInfo info)
        {
            Name = info.Name;
            ReturnType = info.ReturnType;
            Type = VarType.Method;
            Args = info.GetParameters().Select(x => x.ParameterType).ToArray();
            ArgNames = info.GetParameters().Select(x => x.Name).ToArray();
        }

        public MatchingVarInfo(string name, System.Type returnType)
        {
            Name = name;
            ReturnType = returnType;
            Type = VarType.Fixed;
        }

        public override string ToString()
        {
            string v = "";
            switch(Type)
            {
                case VarType.Method:
                    v = Name + "(";
                    for(int i = 0; i < Args.Length; ++i)
                    {
                        v += Args[i].Name + " " + ArgNames[i];
                        if (i < Args.Length - 1)
                            v += ", ";
                    }
                    v += ") : " + ReturnType.Name;
                    break;
                case VarType.Field:
                    v = Name + " : " + ReturnType.Name;
                    break;
                case VarType.Property:
                    v = Name + " : " + ReturnType.Name;
                    break;
                case VarType.Fixed:
                    v = "(Fixed) " + Name + " : ";
                    if (ReturnType != null)
                        v += ReturnType.Name;
                    else
                        v += "?";
                    break;
            }

            return v;
        }
    }

	const float seconds_until_error_check = 0.5f;

	bool _error;
	CSharpExpression _expression;
	WExpression _wexpression;
	SymbolTable _symTable;
	string _lastExpr;
	string _errMsg;
	FrameTimer _timer;
	bool _doErrorCheck;
	List<string> _parsedStringExpressions = new List<string>();
    Dictionary<string, int> _constPropIndexes = new Dictionary<string, int>();
    Dictionary<string, object> _constPropValues = new Dictionary<string, object>();

	public override void OnInspectorGUI ()
	{
		WModuleBase module = target as WModuleBase;

		WApp.Get.RegisterScopeProperties(module.Scope);
		

		if(_lastExpr != module.GetExpression())
		{
			_expression = null;
			_error = false;
			_doErrorCheck = false;
			_timer = new FrameTimer(seconds_until_error_check);
			// WScope.ParseExpressions is used to get multiple expressions out of a single string
			_parsedStringExpressions = WScope.ParseExpressions(module.GetExpression());

            _lastExpr = module.GetExpression();
		}else{
            /*
			if(_timer != null)
			{
				EditorUtility.SetDirty(target);
				_doErrorCheck = !_timer.NextFrame(Time.fixedDeltaTime);
				if(_doErrorCheck)
					_timer = null;
			}
            */
		}

		if(Event.current.type == EventType.layout)
		{
			try
			{
				module.AcquireScope();
				if(_doErrorCheck && _expression == null && !_error)
				{
					_symTable = WCompiler.GenerateSymbolTableFor(module.Scope);
					if(_parsedStringExpressions.Count > 0)
					{
						_wexpression = module.Scope.Generate(_parsedStringExpressions[0]);
						_expression = _wexpression.GenerateCSharp(_symTable);
						_error = false;
					}else{
						_error = true;
					}

					// Consume this if no expression was previously generated (_expression == null)
					// so we don't try to check the semantics of it later
					_doErrorCheck = false;
				}
			}
			catch (System.Exception e) { 
				_error = true;
				_errMsg = e.GetType() + ": " + e.Message + "\n" + e.StackTrace;
			}
		}

        if (Application.HasProLicense())
		    GUI.skin.box.normal.textColor = Color.white;
        else
            GUI.skin.box.normal.textColor = Color.black;

		if(module.Scope == null)
		{
			GUI.color = Color.red;
			GUILayout.Box("Error: No scope found", GUILayout.ExpandWidth(true));
		}
		else
		{
			string scopeName = module.Scope.ToString();
			GUILayout.Box(scopeName, GUILayout.ExpandWidth(true));
		}
	
		if(Application.isPlaying)
		{
			if(module.WillApply && module.Scope.Ready)
			{
				GUI.color = Color.green;
				GUILayout.Box("Evaluating", GUILayout.ExpandWidth(true));
			}else{
				
				GUI.color = Color.red;
				GUILayout.Box("Not Evaluating", GUILayout.ExpandWidth(true));
			}

            if (Application.HasProLicense())
			    GUI.color = Color.white;

		}

		//base.OnInspectorGUI ();

        //SerializedObject obj = new SerializedObject(serializedObject.targetObject);
        //objserializedObject.targetObject
        //Debug.Log();
        SerializedProperty baseProp = serializedObject.GetIterator();

        System.Type moduleType = module.GetType();
        
        while (baseProp.NextVisible(true))
        {
            //SerializedProperty serProp = (SerializedProperty)prop;
            //EditorGUILayout.LabelField(baseProp.propertyType.);
            //Debug.Log(prop);
            string prop_name = (baseProp.name);

            FieldInfo info = moduleType.GetField(prop_name);
            if (info != null)
            {
                object[] attrs = info.GetCustomAttributes(typeof(PopulateFromConstants), true);

                if (attrs.Length > 0)
                {
                    if (!_constPropIndexes.ContainsKey(prop_name))
                    {
                        _constPropIndexes.Add(prop_name, 0);
                        _constPropValues.Add(prop_name, null);
                    }
                    PopulateFromConstants populate = (PopulateFromConstants)attrs[0];

                    Dictionary<string, object> consts = populate.GetConstantValues();
                    string[] options = new string[consts.Count];
                    int i = 0;
                    foreach (KeyValuePair<string, object> kvp in consts)
                    {
                        if (info.GetValue(module) == kvp.Value)
                        {
                            _constPropIndexes[prop_name] = i;
                        }
                        options[i] = kvp.Key;
                        i++;

                    }

                    _constPropIndexes[prop_name] = EditorGUILayout.Popup(baseProp.displayName,_constPropIndexes[prop_name], options);
                    _constPropValues[prop_name] = consts[options[_constPropIndexes[prop_name]]];
                    //info.SetValue(module, );
                    if(_constPropValues[prop_name] is int)
                        baseProp.intValue = (int)_constPropValues[prop_name];
                    else if (_constPropValues[prop_name] is float)
                        baseProp.floatValue = (float)_constPropIndexes[prop_name];
                    else if (_constPropValues[prop_name] is string)
                        baseProp.stringValue = (string)_constPropValues[prop_name];
                    else if (_constPropValues[prop_name] is bool)
                        baseProp.boolValue = (bool)_constPropValues[prop_name];

                    info.SetValue(module, _constPropValues[prop_name]);

                    serializedObject.Update();

                }
                else
                {
                    EditorGUILayout.PropertyField(baseProp);

                }
            }
            else
            {
                EditorGUILayout.PropertyField(baseProp);
            }

        }
        serializedObject.ApplyModifiedProperties();

		
		if(module.Scope != null && !Application.isPlaying)
		{
			DoIntellisense(module);
		}

        if(Application.HasProLicense())
		    GUI.color = Color.white;

		if(_error)
		{
			GUI.color = Color.red;
            GUILayout.Label("Error: " + _errMsg);
            if (Application.HasProLicense())
			    GUI.color = Color.white;
		}

	}

	Vector2 _intellisenseListPos;
	float _intellisenseScrolPos;
    List<MatchingVarInfo> _matchingFields = new List<MatchingVarInfo>();

    class IntellisenseNode
    {
        public bool IsField { get { return _fieldInfo != null; } }
        public bool IsMethod { get { return _methodInfo != null; } }
        public bool IsProperty { get { return _propInfo != null; } }
        public bool IsFixed { get { return _hasObj; } }

        public FieldInfo Field { get { return _fieldInfo; } }
        public MethodInfo Method { get { return _methodInfo; } }
        public PropertyInfo Property { get { return _propInfo; } }
        public object Fixed { get { return _objInfo; } }

        FieldInfo _fieldInfo;
        MethodInfo _methodInfo;
        PropertyInfo _propInfo;
        object _objInfo;
        bool _hasObj;

        public IntellisenseNode(ScopeFieldInfo info) { _fieldInfo = info.Field; }
        public IntellisenseNode(ScopeMethodInfo info) { _methodInfo = info.Method; }
        public IntellisenseNode(ScopePropertyInfo info) { _propInfo = info.Property; }
        public IntellisenseNode(ScopeFixedValue info) { _objInfo = info.Value; _hasObj = true; }

        public IntellisenseNode(FieldInfo info) { _fieldInfo = info; }
        public IntellisenseNode(MethodInfo info) { _methodInfo = info; }
        public IntellisenseNode(PropertyInfo info) { _propInfo = info; }
        public IntellisenseNode(object info) { _objInfo = info; }
    }

    class Intellisense
    {
        public static List<MatchingVarInfo> Sense(string text, WScope scope)
        {
            List<WToken> tokens = WLexer.Default.Lex(text);
            //WExpression expression = new WExpression(scope, tokens);
            MatchingVarInfo parent = null;
            List<MatchingVarInfo> matches = new List<MatchingVarInfo>();

            for(int i = 0; i < tokens.Count; ++i)
            {
                if(tokens[i].Type == WToken.ID)
                {
                    if(parent == null)
                    {
                        matches.AddRange(check_scope_for(scope, tokens[i].Value));
                    }
                    else
                    {
                        matches = check_type_for(parent.ReturnType, tokens[i].Value);
                    }
                }
                
                if(i < tokens.Count - 1 && tokens[i + 1].Value == ".")
                {
                    if (tokens[i].Type == WToken.ID)
                    {
                        parent = GetBestMatch(matches, tokens[i].Value);
                    }
                }

                if(parent == null)
                {
                    break;
                }
            }

            return matches;
        }

        static MatchingVarInfo GetBestMatch(List<MatchingVarInfo> possible_matches, string id)
        {
            for(int i = 0; i < possible_matches.Count; ++i)
            {
                if (possible_matches[i].Name == id)
                    return possible_matches[i];
            }

            return null;
        }

        static List<MatchingVarInfo> check_scope_for(WScope scope, string id)
        {
            string match_id = id.ToLower();

            IEnumerable<ScopeFieldInfo> scopeFields = scope.GetFields();
            IEnumerable<ScopeMethodInfo> scopeMethods = scope.GetMethods();
            IEnumerable<ScopePropertyInfo> scopeProperties = scope.GetProperties();
            IEnumerable<ScopeFixedValue> fixedVals = scope.GetFixedProperties();

            List<MatchingVarInfo> matching_vars = new List<MatchingVarInfo>();

            foreach(ScopeFieldInfo field in scopeFields)
            {
                if(field.Field.Name.ToLower().IndexOf(match_id) == 0)
                {
                    matching_vars.Add(new MatchingVarInfo(field.Field));
                }
            }
            foreach (ScopeMethodInfo method in scopeMethods)
            {
                if (method.Method.Name.ToLower().IndexOf(match_id) == 0)
                {
                    matching_vars.Add(new MatchingVarInfo(method.Method));
                }
            }
            foreach (ScopePropertyInfo prop in scopeProperties)
            {
                if (prop.Property.Name.ToLower().IndexOf(match_id) == 0)
                {
                    matching_vars.Add(new MatchingVarInfo(prop.Property));
                }
            }
            foreach (ScopeFixedValue fixedVal in fixedVals)
            {
                if (fixedVal.Name.ToLower().IndexOf(match_id) == 0)
                {
                    matching_vars.Add(new MatchingVarInfo(fixedVal.Name, fixedVal.Type));
                }
            }

            return matching_vars;
        }

        static List<MatchingVarInfo> check_type_for(System.Type type, string id)
        {
            string match_id = id.ToLower();
            List<MatchingVarInfo> matching_vars = new List<MatchingVarInfo>();

            foreach(var field in type.GetFields())
            {
                if (string.IsNullOrEmpty(id) || field.Name.ToLower().IndexOf(match_id) == 0)
                {
                    matching_vars.Add(new MatchingVarInfo(field));
                }

            }
            foreach (var prop in type.GetProperties())
            {
                if (string.IsNullOrEmpty(id) || prop.Name.ToLower().IndexOf(match_id) == 0)
                {
                    matching_vars.Add(new MatchingVarInfo(prop));
                }
            }
            foreach (var method in type.GetMethods())
            {
                if (string.IsNullOrEmpty(id) || method.Name.ToLower().IndexOf(match_id) == 0)
                {
                    matching_vars.Add(new MatchingVarInfo(method));
                }
            }

            return matching_vars;
        }

    }

	void DoIntellisense(WModuleBase module)
	{
		if(Event.current.type == EventType.layout)
        {
            _doErrorCheck = false;
            try
            {
                _matchingFields = Intellisense.Sense(module.GetExpression().Trim('{','}'), module.Scope);
            }					
            catch(System.Exception e)
			{
				if(_doErrorCheck)
				{
					_error = true;
					_errMsg = e.GetType() + ": " + e.Message + "\n" + e.StackTrace;
				}
			}
            /*
			FieldInfo[] fields = module.GetType ().GetFields();

			if(_doErrorCheck && _expression != null)
			{
				foreach(FieldInfo f in fields)
				{
					object[] attributes = f.GetCustomAttributes(typeof(WAttribute), true);
					foreach(object a in attributes)
					{
						WAttribute casted_attr = (WAttribute)a;
						WIntellisenseMessage msg = casted_attr.IsValid(_expression);
						
						if(!msg.Good)
						{
							_errMsg = msg.Message;
							_error = true;
							break;
						}
					}

					if(_error)
						break;
				}
			}

			_doErrorCheck = false;
            _matchingFields = new List<MatchingVarInfo>();

			if(_parsedStringExpressions.Count > 0)
			{
				string lastMatchingExpr = _parsedStringExpressions[_parsedStringExpressions.Count - 1];

				if(_expressionChanged)
				{
					try
					{
					_lexedTokens = WLexer.Default.Lex(lastMatchingExpr);
					}catch(System.Exception e)
					{
						if(_doErrorCheck)
						{
							_error = true;
							_errMsg = e.GetType() + ": " + e.Message + "\n" + e.StackTrace;
						}
					}
				}

				if(_lexedTokens.Count > 0)
				{
					WToken lastToken = _lexedTokens[_lexedTokens.Count - 1];


                    foreach (ScopeFixedValue info in fixedVals)
                    {
                        if (info.Name.ToLower().IndexOf(lastToken.Value.ToLower()) == 0)
                        {
                            _matchingFields.Add(new MatchingVarInfo(info.Name, info.Type));
                        }
                    }

                    foreach (ScopeMethodInfo info in scopeMethods)
                    {
                        if (info.Method.Name.ToLower().IndexOf(lastToken.Value.ToLower()) == 0)
                        {
                            _matchingFields.Add(new MatchingVarInfo(info.Method));
                        }
                    }

					foreach(ScopeFieldInfo info in scopeFields)
					{
						if(info.Field.Name.ToLower().IndexOf(lastToken.Value.ToLower()) == 0)
						{
							_matchingFields.Add(new MatchingVarInfo(info.Field));
						}
					}

					foreach(ScopePropertyInfo info in scopeProperties)
					{
						if(info.Property.Name.ToLower().IndexOf(lastToken.Value.ToLower()) == 0)
						{
							_matchingFields.Add(new MatchingVarInfo(info.Property));
						}
					}
				}
			}*/
		}

		GUILayout.Space(10);
        foreach (MatchingVarInfo f in _matchingFields)
		{
			GUILayout.Box(f.ToString(), GUILayout.ExpandWidth(true));
		}


		if(Event.current.type == EventType.keyDown)
		{
			if(Event.current.keyCode == KeyCode.Return)
			{

			}
		}


			/*
			Rect lastRect = GUILayoutUtility.GetLastRect();
			lastRect.width = 120;
*/


			/*
			lastRect.height = matchingFields.Count * 20;
			GUI.BeginGroup(lastRect, GUI.skin.box);
			GUI.BeginScrollView(new Rect(0,0,lastRect.width, 60),
			                    _intellisenseListPos,
			                    new Rect(0,_intellisenseScrolPos,lastRect.width,60));
			 
			int i = 0;
			foreach(string f in matchingFields)
			{
				GUI.Box(new Rect(0,20 * i,lastRect.width, 20), f);
				i++;
			}
			//GUI.Box(new Rect(0,0,20,300),"beans2mnbmnbmnbmnbmnbmnbmnbmnbbeans2mnbmnbmnbmnbmnbmnbmnbmnb");
			GUI.EndScrollView();
			_intellisenseScrolPos = GUI.VerticalScrollbar(new Rect(0,0,20, 60), _intellisenseScrolPos, 1, 60, 0);
			GUI.EndGroup();


			_intellisenseListPos.y = _intellisenseScrolPos;
			*/

	}
}
