﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class WApp 
{

	static WApp _app;

	public static WApp Get
	{
		get
		{
			if(_app == null)
			{
                _app = new WApp();
			}

			return _app; 
		}
	}

	public bool Setup{ get{ return _setup; } }

 	bool _setup;
	List<WScope> _scopes;

	public void SetupApp()
	{		
		_scopes = new List<WScope>();
		Debug.Log("Initializing Wrangular app..");
		var scopes = GameObject.FindObjectsOfType<WScope>();
		foreach (WScope scope in scopes)
		{
			if(scope.Ready) continue;
			WScope parent = scope.transform.TraverseHierarchyFor<WScope>(true);
			try
			{
				scope.OnScopeRegistered(this, parent);
			}catch(System.Exception e)
			{
				Debug.LogError("Failed to register scope " + scope);
				Debug.LogException(e, scope);
			}
			_scopes.Add (scope);
		}
		Debug.Log("Wrangular initialized.");
		_setup = true;
	}

	public void RegisterScopeUnder(WScope scope, WScope parent)
	{
		scope.OnScopeRegistered(this, parent);
	}

	public void RegisterScope(WScope scope)
	{
		WScope parent = scope.transform.TraverseHierarchyFor<WScope>(true);
		scope.OnScopeRegistered(this, parent);
	}

	public void RegisterScopeProperties(WScope scope)
	{
		if(scope != null)
		{
			WScope parent = scope.transform.TraverseHierarchyFor<WScope>(true);
			scope.OnScopePropertySetup(this, parent);
		}
	}
}
