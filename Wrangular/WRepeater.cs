﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class WRepeater : WScope 
{
	public string Value;
	public string RepeaterName = "Item";
	public bool SetReadyToUI;

	List<WRepeaterChild> _children = new List<WRepeaterChild>();
	IEnumerable _list;

	List<MonoBehaviour> _components;
	WBindingSettings _bindSettings;

	public List<WRepeaterChild> GetChildren()
	{
		return _children;
	}

	public override void OnScopePropertySetup(WApp app, WScope parent)
	{
		AddFixedProp(RepeaterName, null);
		base.OnScopePropertySetup(app, parent);
	}

	protected override void OnScopeReady()
	{
		_components = GetComponents<MonoBehaviour>().Where(x => x != this).ToList();
		_components.ForEach(x => x.enabled = false);
		_bindSettings = new WBindingSettings()
		{
			Apply = Apply,
			BindSuccess = OnSuccess,
			PostApply = OnPostApply
		};
		if (ParentScope != null)
			ParentScope.Bind(this, "{" + Value + "}", _bindSettings);
		else
			throw new System.Exception("No parent scope found.");

		gameObject.SetActive(false);

	}

    void OnSuccess(Dictionary<string, object> vals)
    {
    }

	protected override void OnApplyBinding(WBinding bindingToApply)
	{
		if (ParentScope.Hijacker == null)
		{
			base.OnApplyBinding(bindingToApply);
		}
		else
		{

			if(bindingToApply is WRepeaterChild.WChildBinding)
			{
				WRepeaterChild.WChildBinding childBinding = (WRepeaterChild.WChildBinding)bindingToApply;
				int childIndex = -1;

				for (int i = 0; i < _children.Count; ++i)
				{
					if (_children[i] == childBinding.ChildScope)
					{
						childIndex = i;
						break;
					}
				}

				AddFixedProp(RepeaterName, _children[childIndex].CachedRepeaterItem);
			}

			ParentScope.Hijacker.ApplyBinding(bindingToApply);
			//ApplyParentBinding(bindingToApply);
		}
	}

	protected virtual void Apply(string propName, object val)
	{
		if(val is IEnumerable)
		{
			_list = (IEnumerable)val;
			ClearProps();
            List<WRepeaterChild> _newChildren = new List<WRepeaterChild>();

			int i = 0;
			foreach(object o in _list)
			{
				bool foundExisting = i < _children.Count;
				if(foundExisting)
				{
                    if (!_children[i].RepeaterItem.Equals(o))
                        _children[i].SendMessage("OnChildValueChanged", this, SendMessageOptions.DontRequireReceiver);

                    _children[i].ApplyChild(RepeaterName, o, i);

                    _children[i].SendMessage("OnChildApplied", this, SendMessageOptions.DontRequireReceiver);
					i++;
					continue;
				}
				else{

					Vector3 originalScale = transform.localScale;
					Vector3 originalPos = transform.localPosition;

					GameObject obj = (GameObject)GameObject.Instantiate(gameObject);
					DestroyImmediate(obj.GetComponent<WRepeater>());
					WRepeaterChild child = obj.AddComponent<WRepeaterChild>();
					child.SetReadyToGameUI = SetReadyToUI;
					_children.Add (child);
					child.transform.SetParent(transform.parent, false);

					App.RegisterScopeUnder(child, this);

					// The child has a custom property called whatever RepeaterName is set to
					child.ApplyChild(RepeaterName, o, i);
					
					WScope[] childScopes = child.transform.TraverseDownFor<WScope>(true);

					foreach(WScope childScope in childScopes)
					{
						App.RegisterScopeUnder(childScope, child);
					}

					
					foreach(MonoBehaviour c in _components)
					{
						if(obj.GetComponent(c.GetType()) != null)
						{
							((MonoBehaviour)obj.GetComponent(c.GetType())).enabled = true;
						}
					}

					obj.transform.localPosition = originalPos;
					obj.transform.localScale = originalScale;

                    WModuleBase[] modules = obj.transform.TraverseDownFor<WModuleBase>();
                    for (int k = 0; k < modules.Length; ++k)
                    {
                        modules[k].ManualSetup();
                    }
                    child.UpdateChild();
                    child.ApplyAllEveryUpdate = ApplyAllEveryUpdate;
                    obj.SetActive(true);

                    OnAddComponentsToChild(child);

                    _newChildren.Add(child);

				}
				i++;
			}

			while(i < _children.Count)
			{
				_children[i].SendMessage("OnChildDestroyed", this, SendMessageOptions.DontRequireReceiver);
				Destroy (_children[i].gameObject);
				_children.RemoveAt(i);
				i++;
			}

            foreach(WRepeaterChild child in _newChildren)
                child.SendMessage("OnChildCreated", this, SendMessageOptions.DontRequireReceiver);


		}

	}

    protected virtual void OnAddComponentsToChild(WRepeaterChild child)
    {

    }

	protected virtual void OnPostApply()
	{
		//Loading = false;
	}
	
	public override string ToString ()
	{
		return base.ToString() + " -> " + RepeaterName;
	}
}
