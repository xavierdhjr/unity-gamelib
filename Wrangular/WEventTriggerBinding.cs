﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof(UnityEngine.EventSystems.EventTrigger))]
public class WEventTriggerBinding : WModuleBase 
{
    public string Expression;
    public UnityEngine.EventSystems.EventTriggerType Trigger;
    public bool ListenForRightClick;
    public bool ListenForLeftClick;

    public override bool IsExpressionMultipleExpressions
    {
        get { return false; }
    }

    protected override string ExpressionString()
    {
        return "{" + Expression + "}";
    }

    WExpression _expression;
    UnityEngine.EventSystems.EventTrigger _trigger;
    bool _badEvent = false;

    protected override void Start()
    {
        AcquireScope();
        _expression = Scope.Generate(Expression);

        _trigger = GetComponent<UnityEngine.EventSystems.EventTrigger>();
        
        
        UnityEngine.EventSystems.EventTrigger.Entry entry = new UnityEngine.EventSystems.EventTrigger.Entry()
        {
            eventID = Trigger,
            callback = new UnityEngine.EventSystems.EventTrigger.TriggerEvent()
        };
        entry.callback.AddListener(OnEvent);
        _trigger.triggers.Add(entry);

    }

    public void OnEvent(UnityEngine.EventSystems.BaseEventData e)
    {
        UnityEngine.EventSystems.PointerEventData pointerEvent = (UnityEngine.EventSystems.PointerEventData)e;

        bool eval = true;
        if (pointerEvent != null && Trigger == UnityEngine.EventSystems.EventTriggerType.PointerClick)
        {
            eval = false;
            if (ListenForRightClick && pointerEvent.button == UnityEngine.EventSystems.PointerEventData.InputButton.Right)
            {
                eval = true;
            }else if(ListenForLeftClick && pointerEvent.button == UnityEngine.EventSystems.PointerEventData.InputButton.Left)
            {
                eval = true;
            }
        }

        if (_badEvent) return;
        try
        {
            if(eval)
                _expression.Evaluate();
        }catch(System.Exception ex)
        {
            _badEvent = true;
            Debug.LogError("Error while firing event on " + gameObject + " (" + Scope + "). Dumping expression.");
            _expression.Dump();
            Debug.LogException(ex);
        }
    }

    protected override void Apply(string propName, object val)
    {

    }
}

