﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;

[System.Serializable]
public class TestExamples
{
    public bool TextColor;
    public bool Model;
    public bool FillBar;
    public bool GridRepeater;
    public bool Disabled;
    public bool FillSprite;
    public bool PrefabSwap;
}

public class ExampleInventory
{
    public List<ExampleInventoryItem> Items;

    static string[] ITEM_NAMES = new string[]{
        "Sword of Justice",
        "Sword of Injustice",
        "Greatsword",
        "Longsword",
        "Hamburger",
        "Ketchup",
        "Snapple",
        "iPhone",
        "Staff of Wielding",
        "Staff of Yielding",
        "Staff of Fielding"
    };

    public ExampleInventory()
    {
        Items = new List<ExampleInventoryItem>();
    }

    public void AddItem()
    {
        Items.Add(new ExampleInventoryItem()
        {
            Name = ITEM_NAMES[Random.Range(0, ITEM_NAMES.Length)],
            Quantity = Random.Range(1, 100)
        });
    }

    public void RemoveLastItem()
    {
        Items.RemoveAt(Items.Count - 1);
    }
}

public class ExampleInventoryItem
{
    public string Name;
    public int Quantity;
}

public class TestController : BaseController 
{
	public string SomeValue;
	public long UpdateTicks;
	public float AverageTicks;
	public long TotalTicks;
	public float Samples;
    public float SomePercentage;
    public Color SomeColor;
    public float T;
    public float TSeconds = 5;
    public bool SomeButtonEnabled;
    public int TestInt = 3;

    public List<string> Prefabs = new List<string>()
    {
        "SwapObjects/Cube",
        "SwapObjects/Capsule",
        "SwapObjects/Sphere"
    };
    public int CurrentPrefabIndex;

    public string PathToPrefab { get { return Prefabs[CurrentPrefabIndex % Prefabs.Count]; } }

    public string EnabledText
    {
        get
        {
            if (SomeButtonEnabled)
                return "Yay I am enabled!";
            return "Oh no- I am disabled!";
        }
    }
    public Color EnableColor
    {
        get
        {
            if (SomeButtonEnabled)
                return Color.black;
            return Color.red;
        }
    }
    public string ToggleText
    {
        get
        {
            if (SomeButtonEnabled)
                return "Disable that button ->";
            return "Enable that button ->";
        }
    }

    public TestExamples Examples;
    public ExampleInventory SomeInventory;

    void Awake()
    {
        SomeInventory = new ExampleInventory();
    }

	protected override void FixedUpdate()
	{

		Stopwatch sw = new Stopwatch();
		sw.Start();
		base.FixedUpdate();
		sw.Stop();
		//UnityEngine.Debug.Log(name + ": " + sw.ElapsedTicks);
		UpdateTicks = sw.ElapsedTicks;
		TotalTicks += UpdateTicks;
		Samples++;
		AverageTicks = TotalTicks / Samples;
	}

	protected override void Update()
	{
		base.Update();
        T += Time.deltaTime;

		SomeValue = "";
		for (int i = 0; i < 23; ++i)
		{
			SomeValue += Random.value.ToString();
		}

        SomeColor = new Color(Mathf.Sin(T), Mathf.Cos(T), Mathf.Tan(T));

        SomePercentage = (T % TSeconds) / TSeconds;
	}

    public int someFun2(int k)
    {
        return k * k;
    }

    public bool someFun(int i, int k)
    {
        return i == k;
    }

    public void ToggleButton()
    {
        SomeButtonEnabled = !SomeButtonEnabled;
    }

    public float ToPercent(float i)
    {
        return Mathf.Round(i * 100);
    }

    public void NextPrefab()
    {
        CurrentPrefabIndex++;
    }

    public void PrevPrefab()
    {
        CurrentPrefabIndex--;
    }
}
