﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class WXboxMenuController : WController 
{
    public int PlayerId;
    public string IsActiveExpression;
    public bool IsActive;
    public WRepeater Repeater;
    public List<WXboxMenuItem> Items;

    int _selectedItem = 0;
    int _totalCount;
    MenuNavigation _navigation = new MenuNavigation();

    public bool IsItemSelected(int index)
    {
        return _selectedItem == index;
    }

    protected override void OnControllerSetup()
    {
        WBindingSettings settings = new WBindingSettings() { Apply = Apply };

        ParentScope.Bind(this, "{" + IsActiveExpression + "}", settings);
    }

    protected void Apply(string propName, object val)
    {
        bool is_active = (bool)val;

        // Reset after being act
        if(IsActive != is_active)
        {
            _navigation.Reset();
            Update();
        }

        IsActive = is_active;
    }

    void Update()
    {

        if (InputSystem.Get.GetCurrentFilter() == InputSystem.FILTER_STANDARD && IsActive)
        {
            int prevItem = _selectedItem;

            int repeater_count = 0;
            
            if(Repeater != null) repeater_count = Repeater.GetChildren().Count;

            if (prevItem >= repeater_count && Items.Count > 0)
            {
                Items[prevItem - repeater_count].IsSelected = false;
            }   

            _totalCount = Items.Count + repeater_count;

            XboxControllerInput input = (XboxControllerInput)InputSystem.Get.GetPlayerInput(PlayerId);

            float vert = input.GetXboxAxis(XboxControllerInput.L_AXIS_VERTICAL);

            _selectedItem = _navigation.UpdateNavigation(_totalCount, vert, Time.deltaTime);

            if (Repeater != null && _selectedItem < repeater_count)
            {
                List<WRepeaterChild> children = Repeater.GetChildren();
                WRepeaterChild currentChild = children[_selectedItem];

                Button currBtn = currentChild.gameObject.GetComponent<Button>();

                if (currBtn != null)
                {
                    currBtn.Select();
                }

                WClick attachedClick = currentChild.GetComponent<WClick>();

                if (attachedClick != null)
                {
                    if (input.GetXboxButtonDown(XboxControllerInput.BTN_GREEN))
                    {
                        attachedClick.Click();
                    }
                }
            }
            else if(Items.Count > 0)
            {
                Items[_selectedItem - repeater_count].IsSelected = true;
            }
        }
    }

}
