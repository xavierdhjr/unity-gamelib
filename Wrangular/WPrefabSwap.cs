﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class WPrefabSwap : WModuleBase 
{
    [Wrangular.Attributes.Returns(typeof(string))]
    public string PathModel;

    Dictionary<string, object> _vals;
    bool _needPostApply;

    public override bool IsExpressionMultipleExpressions
    {
        get { return true; }
    }

    protected override string ExpressionString()
    {
        return PathModel;
    }

    protected override WBindingSettings CreateBindSettings()
    {
        WBindingSettings settings = base.CreateBindSettings();
        settings.BindSuccess = OnSuccess;
        settings.PostApply = OnPostApply;
        return settings;
    }

    protected void OnSuccess(Dictionary<string, object> vals)
    {
        _vals = vals;
    }

    protected override void Apply(string propName, object val)
    {
        if (_vals.ContainsKey(propName))
        {
            if (_vals[propName] != val)
            {
                _vals[propName] = val;
                _needPostApply = true;
            }
        }
    }


    void OnPostApply()
    {
        if (_needPostApply)
        {
            _needPostApply = false;
            string outputText = PathModel;

            foreach (KeyValuePair<string, object> kvp in _vals)
            {
                string v = "NULL";
                if (kvp.Value != null)
                {
                    v = kvp.Value.ToString();
                }

                string output = v;

                outputText = outputText.Replace("{" + kvp.Key + "}", output);
            }

            SwapPrefab(outputText);
        }
    }

    void SwapPrefab(string pathToNewPrefab)
    {
        Object o = Resources.Load(pathToNewPrefab);

        if (transform.childCount > 0)
            Destroy(transform.GetChild(0).gameObject);

        if (o == null)
        {
            GameObject emptyObj = new GameObject("No Prefab Loaded");
            emptyObj.transform.parent = this.transform;
            emptyObj.transform.localPosition = Vector3.zero;
            return;
        }

        GameObject obj = GameObject.Instantiate(o) as GameObject;

        obj.transform.parent = this.transform;
        obj.transform.localPosition = Vector3.zero;
    }
}
