﻿using UnityEngine;
using System.Collections;

public class WAnimOnEvent : WModuleBase
{
    public enum AnimStyle
    {
        Trigger,
        Bool,
        Float,
        Int
    }

    public string AnimName;
    
    public string AnimValueExpression;
    [PopulateFromConstants(typeof(GameEvents), "")]
    public string Event;


    public override bool IsExpressionMultipleExpressions
    {
        get { return false; }
    }

    protected override string ExpressionString()
    {
        return "{" + AnimValueExpression + "}";
    }

    protected override void Apply(string propName, object val)
    {

    }
}
