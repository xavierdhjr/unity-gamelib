﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Wrangular.Compiler;

public class WIntellisenseMessage
{
	public bool Good;
	public string Message;

	public static WIntellisenseMessage Fail(string message)
	{
		return new WIntellisenseMessage()
		{
			Good = false,
			Message = message
		};
	}

	public static WIntellisenseMessage Success()
	{
		return new WIntellisenseMessage(){ Good = true };
	}
}

namespace Wrangular.Attributes
{

	public abstract class WAttribute : System.Attribute
	{
		public abstract WIntellisenseMessage IsValid(CSharpExpression expression);
	}


	public class Returns : WAttribute
	{
		System.Type _returnType;

		public Returns(System.Type returnType)
		{
			_returnType = returnType;
		}

		public override WIntellisenseMessage IsValid (CSharpExpression expression)
		{
			if(expression.Type == _returnType)
				return WIntellisenseMessage.Success();
			return WIntellisenseMessage.Fail("Expected expression to return type " + _returnType);
		}
	}
	
	public class Method : Returns
	{
		public Method(System.Type returnType) : base(returnType)
		{

		}

		public override WIntellisenseMessage IsValid (CSharpExpression expression)
		{
			if(expression is CSharpFuncExpression)
			{
				return base.IsValid(expression);
			}

			return WIntellisenseMessage.Fail("Expression is not a function.");
		}
	}

	public class Arg : WAttribute
	{
		System.Type _argType;
		int _argNo;

		public Arg(int argNo, System.Type argType)
		{
			_argNo = argNo;
			_argType = argType;
		}

		public override WIntellisenseMessage IsValid (CSharpExpression expression)
		{
			if(expression is CSharpFuncExpression)
			{
				CSharpFuncExpression func = (CSharpFuncExpression)expression;
				if(func.ArgCount < _argNo)
					return WIntellisenseMessage.Fail("Expected " + func.ArgCount + " arguments.");
				if(func.Args.ArgExpressions[_argNo].Type != _argType)
				{
					return WIntellisenseMessage.Fail("Expected arg " 
					                                 + _argNo + " to be of type " 
					                                 + func.Args.ArgExpressions[_argNo].Type
					                                 );
				}

				return WIntellisenseMessage.Success();
			}

			return WIntellisenseMessage.Fail("Expression is not a function.");
		}
	}
}