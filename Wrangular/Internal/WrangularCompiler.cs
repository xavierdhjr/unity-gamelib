﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Wrangular.Compiler
{
	public class InferredType
	{
		public string Id;
		public System.Type Type;
	}

	public class SymbolTable
	{

		/// <summary>
		/// Incremented to uniquely identify a symbol across all tables
		/// </summary>
		static int _currentId = 0;

		/// <summary>
		/// The scope tied to this table
		/// </summary>
		public WScope CurrentScope;

		public SymbolTable ParentTable;
		public List<SymbolTable> ChildTables = new List<SymbolTable>();

		/// <summary>
		/// Different from the class name of the scope- this is the name of the compiled class.
		/// </summary>
		public string CurrentClassName;

		List<Symbol> _symbols = new List<Symbol>();
		List<InferredType> _inferredTypes = new List<InferredType>();

		/// <summary>
		/// Creates a new table underneath this one and ties it to a given scope. 
		/// </summary>
		public SymbolTable NewChildTable(WScope scope)
		{
			SymbolTable t = new SymbolTable();
			t.CurrentScope = scope;
			t.ParentTable = this;
			t.ChildTables = new List<SymbolTable>();

			ChildTables.Add(t);

			return t;
		}

		/// <summary>
		/// Returns all symbol tables underneath this table so they are in list format 
		/// (recursively goes through each child table and their children)
		/// </summary>
		public List<SymbolTable> GetChildrenTablesRecursive()
		{
			return InternalGetChildrenTableRecursive(this);
		}

		List<SymbolTable> InternalGetChildrenTableRecursive(SymbolTable currentTable)
		{
			List<SymbolTable> tables = new List<SymbolTable>();

			foreach(SymbolTable t in currentTable.ChildTables)
			{
				tables.Add(t);
				tables.AddRange(InternalGetChildrenTableRecursive(t));
			}

			return tables;
		}

		public List<Symbol> GetAllSymbols()
		{
			return new List<Symbol>(_symbols);
		}

		public void AddInferredType(string id, System.Type type)
		{
			if (_inferredTypes.Find(x => x.Id == id) != null)
				throw new System.Exception("Duplicate inferred type found for '" + id + "'");

			_inferredTypes.Add(new InferredType()
			{
				Id = id,
				Type = type
			});
		}

		public bool HasInferredType(string id)
		{
			bool hasInferredType = InternalGetInferredType(id) != null;

			if (hasInferredType)
				return true;
			else if (ParentTable != null)
				return ParentTable.HasInferredType(id);
			return false;
		}

		public System.Type GetInferredType(string id)
		{
			InferredType t = InternalGetInferredType(id);
			if (t == null)
			{
				if (ParentTable != null)
					return ParentTable.GetInferredType(id);

				Debug.Log("*** Dumping table");
				Debug.Log(DumpInfo());
				throw new System.Exception("No inferred type could be found for '" + id + "'");
			}
			return t.Type;
		}

		InferredType InternalGetInferredType(string id)
		{

			InferredType t = _inferredTypes.Find(x => x.Id == id);
			return t;
		}

		public Symbol NewScopeSymbol(string name, string type, Transform t)
		{

			Symbol s = NewSymbol(name, type, t);
			s.IsScope = true;
			s.IsModule = false;
			return s;
		}

		public Symbol NewMethod(string name, string returnType, List<string> args)
		{
			foreach (Symbol sym in _symbols)
			{
				if (sym.Name == name)
				{
					if (!sym.IsFunction)
						throw new System.Exception("Ambiguous method name '" + name + "' with existing var '" + sym.Name + "'");
					if (sym.Args.Count == args.Count)
						throw new System.Exception("Redeclaration of existing method '" + name + "'");
				}
			}

			_currentId++;
			Symbol s = new Symbol()
			{
				Id = _currentId,
				Type = returnType,
				IsFunction = true,
				Args = args,
				Name = name
			};
			_symbols.Add(s);
			return _symbols[_symbols.Count - 1];
		}

		public Symbol NewVar(string name, string type)
		{
			if (InternalGetSymbol(name) != null)
			{
				throw new System.Exception("Duplicate symbol '" + name + "' found.");
			}
			_currentId++;
			_symbols.Add(new Symbol()
			{
				Name = name,
				Type = type,
				Id = _currentId,
				IsModule = false
			});

			Symbol s = _symbols[_symbols.Count - 1];
			return s;
		}

		public Symbol NewSymbol(string name, string type, Transform t)
		{
			Symbol existing_sym = InternalGetSymbol(name);
			if (existing_sym != null)
			{
				Debug.Log("*** Dumping info");
				Debug.Log(DumpInfo());
				throw new System.Exception("Duplicate symbol '" + name + "' found. Previously declared on '" + existing_sym.Transform + "' (" + existing_sym.ToTransformString() + ")" );
			}
			_currentId++;
			_symbols.Add(new Symbol()
			{
				Name = name,
				Type = type,
				Id = _currentId,
				Transform = t,
				IsModule = true
			});

			Symbol s = _symbols[_symbols.Count - 1];
			return s;
		}


		public Symbol NewTempSymbol(string name, string type, Transform t)
		{
			return new Symbol() { Name = name, Type = type, Transform = t };
		}

		public static string MakeSymbolName(WScope scope)
		{
			return scope.GetType().Name + "&" + scope.transform.GetTransformPath();
		}

		public static string MakeSymbolName(WModuleBase module)
		{
			return module.GetType().Name + "&" + module.transform.GetTransformPath();
		}

		public Symbol GetSymbol(WScope scope)
		{
			return GetSymbol(MakeSymbolName(scope));
		}

		public Symbol GetSymbol(string name)
		{

			Symbol s = InternalGetSymbol(name, true);
			if (s == null)
			{
				Debug.Log("*** Dumping table info");
				Debug.Log(DumpInfo());
				throw new System.Exception("Undefined symbol '" + name + "'");
			}
			return s;
		}

		public string DumpInfo()
		{
			Dumper d = new Dumper("Symbol Table " + CurrentScope);
			d.AddLine("Symbols");
			d.AddDivider();
			foreach(Symbol s in _symbols)
			{
				if (s.IsFunction)
					d.AddLine("** Function");
				if (s.IsModule)
					d.AddLine("** Module");
				if (s.IsScope)
					d.AddLine("** Scope");
				d.AddProp("name", s.Name);
				d.AddProp("type", s.Type);
				d.AddProp("expr", s.Expression);
				d.AddLine("-------------------");
			}
			d.AddDivider();
			d.AddLine("Inferred Types");
			d.AddDivider();
			foreach(InferredType t in _inferredTypes)
			{
				d.AddProp("Id", t.Id);
				d.AddProp("Type", t.Type);
				d.AddLine("-------------------");
			}

			d.AddDivider();
			if (ParentTable != null)
			{
				d.AddLine("########### Parent Scope ###########");
				d.Add(ParentTable.DumpInfo());
			}
			return d.GetDump();
		}

		Symbol InternalGetSymbol(string name, bool searchParent = false)
		{

			Symbol s = _symbols.Find(x => x.Name == name);

			if(s == null && searchParent)
			{
				if(ParentTable != null)
				{
					return ParentTable.InternalGetSymbol(name);
				}
			}

			return s;
		}
	}

	public class Symbol
	{
		/// <summary>
		/// The string representing the unescaped non-code friendly name of this symbol
		/// </summary>
		public string Name;

		/// <summary>
		/// The string representing the System.Type of this symbol
		/// </summary>
		public string Type;

		/// <summary>
		/// The string representing the expression tied to this symbol.
		/// </summary>
		public string Expression;

		/// <summary>
		/// A unique integer that identifies this symbol
		/// </summary>
		public int Id;

		/// <summary>
		/// 
		/// </summary>
		public Transform Transform;
		public bool IsScope;
		public bool IsFunction;
		public bool IsModule;
		public List<string> Args;

		public override string ToString()
		{
			return Name.Replace("/", "_").Replace(" ", "").Replace("-", "_").Replace("&", "__").Replace("#","_");
		}

		public string ToTransformString()
		{
			return Name.Substring(Name.IndexOf('&') + 1);
		}
	}

	public abstract class ASTNode
	{
		public int depth;
		public abstract Symbol CompileCSharp(SymbolTable table, TextWriter writer);

		public string indent()
		{
			string i = "";
			for (int k = 0; k < depth; ++k)
				i += "\t";

			return i;
		}
	}

	public class SymbolDeclareUsingDiffTableNode : SymbolDeclareNode
	{
		SymbolTable _tableToUse;

		public SymbolDeclareUsingDiffTableNode(Symbol s, SymbolTable t)
			: base(s)
		{
			_tableToUse = t;
		}

		public override Symbol CompileInitialization(SymbolTable table, TextWriter writer)
		{
			return base.CompileInitialization(_tableToUse, writer);
		}

		public override Symbol CompileCSharp(SymbolTable table, TextWriter writer)
		{
			return base.CompileCSharp(_tableToUse, writer);
		}
	}

	public class SymbolDeclareNode : VarDeclareNode
	{
		public SymbolDeclareNode(string name, string type)
			: base(name, type)
		{
		}

		public SymbolDeclareNode(Symbol s) : base(s.Name, s.Type)
		{

		}

		public override Symbol CompileInitialization(SymbolTable table, TextWriter writer)
		{
			Symbol s = base.CompileInitialization(table, writer);

			writer.Write(indent() + s.ToString() + " = TransformExtensions.FindTransformByPath(\"" + s.ToTransformString() + "\")");

			if (s.Type == "GameObject")
			{
				writer.Write(".gameObject;");
			}
			else
			{
				writer.Write(".GetComponent<"
								 + s.Type
								 + ">();");
			}
			writer.WriteLine();

			return s;
		}
	}

	public class VarDeclareDefaultConstructorNode : VarDeclareNode
	{
		public VarDeclareDefaultConstructorNode(string name, string type)
			: base(name, type)
		{
		}

		public override Symbol CompileInitialization(SymbolTable table, TextWriter writer)
		{
			Symbol s = base.CompileInitialization(table, writer);

			writer.WriteLine(indent() + s.ToString() + " = new " + s.Type + "();");

			return s;
		}
	}

	public class VarDeclareNode : ASTNode
	{
		string _name;

		public VarDeclareNode(string name, string type)
		{
			_name = name;
		}
		public override Symbol CompileCSharp(SymbolTable table, TextWriter writer)
		{
			try
			{
				Symbol s = table.GetSymbol(_name);
				writer.WriteLine(indent() + "public " + s.Type + " " + s.ToString() + ";");
				return s;
			}
			catch (System.Exception)
			{
				writer.WriteLine(indent() + "// Undefined var '" + _name + "'");
			}
			return null;
		}

		public virtual Symbol CompileInitialization(SymbolTable table, TextWriter writer)
		{
			Symbol s = table.GetSymbol(_name);
			return s;
		}
	}

	public class AwakeNode : ASTNode
	{
		List<ASTNode> _awakeBodyNodes = new List<ASTNode>();
		List<VarDeclareNode> _varNodes = new List<VarDeclareNode>();

		public void AddToBody(ASTNode node)
		{
			_awakeBodyNodes.Add(node);

		}

		public void AddToVars(VarDeclareNode node)
		{
			_varNodes.Add(node);
		}

		public override Symbol CompileCSharp(SymbolTable table, TextWriter writer)
		{
			List<Symbol> declared = new List<Symbol>();

			Symbol scopeSymbol = table.GetSymbol(table.CurrentScope);

			writer.WriteLine(indent() + "public " + scopeSymbol.Type + " " + scopeSymbol.ToString() + ";");
			writer.WriteLine();
			foreach (VarDeclareNode node in _varNodes)
			{
				node.depth = depth;
				declared.Add(node.CompileCSharp(table, writer));
			}
			writer.WriteLine();
			writer.WriteLine(indent() + "protected override void OnReady()");
			writer.WriteLine(indent() + "{");

			depth += 1;
			writer.WriteLine(indent() + "// Current Scope");
			writer.WriteLine(indent() + scopeSymbol.ToString() + " = GetComponent<" + scopeSymbol.Type + ">();");
			writer.WriteLine();

			foreach (VarDeclareNode node in _varNodes)
			{
				node.depth = depth;
				node.CompileInitialization(table, writer);
			}
			depth -= 1;

			foreach (ASTNode node in _awakeBodyNodes)
			{
				node.depth = depth + 1;
				node.CompileCSharp(table, writer);

			}

			writer.WriteLine(indent() + "}");
			return null;

		}
	}

	public class ClassNode : ASTNode
	{
		string _className;

		List<ASTNode> _other;

		public ClassNode(string className)
		{
			_className = className;
			_other = new List<ASTNode>();
		}

		public void AddToClassDefinition(ASTNode ast)
		{
			_other.Add(ast);
		}

		public override Symbol CompileCSharp(SymbolTable table, TextWriter writer)
		{
			writer.WriteLine(indent() + "public class " + _className + ": CompiledControllerBase");
			writer.WriteLine(indent() + "{");

			foreach (ASTNode a in _other)
			{
				a.depth = depth + 1;
				a.CompileCSharp(table, writer);
				//Symbol methodSym = m.CompileCSharp(table, writer);
			}


			writer.WriteLine(indent() + "}");

			return null;
		}

	}

	public class ApplyBindingNode : ASTNode
	{
		void CompileTable(SymbolTable table, TextWriter writer)
		{

			Dictionary<string, List<Symbol>> symbolExpressionMapping = new Dictionary<string, List<Symbol>>();
			foreach (Symbol s in table.GetAllSymbols())
			{
				if (string.IsNullOrEmpty(s.Expression))
				{
					Debug.Log("Empty expression for symbol " + s.Name);
					continue;
				}

				string expr = s.Expression.Trim('{', '}');
				if (!symbolExpressionMapping.ContainsKey(expr))
					symbolExpressionMapping.Add(expr, new List<Symbol>() { s });
				else
					symbolExpressionMapping[expr].Add(s);
			}
			writer.WriteLine();
			writer.WriteLine(indent() + "switch(bindingToApply.NameHash)");
			writer.WriteLine(indent() + "{");
			{
				depth += 1;
				foreach (KeyValuePair<string, List<Symbol>> kvp in symbolExpressionMapping)
				{
					CSharpExpression csharp_expression = null;
					try
					{
						csharp_expression = table.CurrentScope.Generate(kvp.Key).GenerateCSharp(table);
					}
					catch (System.Exception e)
					{
						writer.WriteLine(indent() + "// Failed to generate expression '" + kvp.Key + "'");
						Debug.LogError("Failed to generate expression '" + kvp.Key + "'");
						Debug.LogException(e);
						continue;
					}

					writer.WriteLine(indent() + "// " + csharp_expression.Type + " expression");
					writer.WriteLine(indent() + "case " + WBinding.Hash(kvp.Key) + ": // " + kvp.Key);
					if (string.IsNullOrEmpty(kvp.Key))
					{
						writer.WriteLine(indent() + "// null expression");
						continue;
					}

					depth += 1;
					foreach (Symbol s in kvp.Value)
					{

						try
						{
							if (csharp_expression.Type != typeof(void))
							{
								writer.WriteLine(indent() + "if(" + csharp_expression.ToNullCheckString() + "){");
								string tempobjName = "tempval_" + s.ToString();
								depth += 1;
								writer.WriteLine(indent() + "object " + tempobjName + " = " + csharp_expression.ToExpressionString() + ";");
								writer.WriteLine(indent() + "bindingToApply.ManualApply(" + tempobjName + ");");
								depth -= 1;
								writer.WriteLine(indent() + "}else{");
								depth += 1;
								writer.WriteLine(indent() + "bindingToApply.ManualApply(null);");
								depth -= 1;
								writer.WriteLine(indent() + "}");
								writer.WriteLine();

							}
						}
						catch (System.Exception e)
						{

							writer.WriteLine(indent() + "// Failed to generate expression '" + s.Expression + "'");
							Debug.LogError("Failed to generate expression '" + s.Expression + "'");
							Debug.LogException(e);
						}

					}
					writer.WriteLine(indent() + "break;");
					depth -= 1;
				}
				depth -= 1;
			}
			writer.WriteLine(indent() + "}");
		}

		public override Symbol CompileCSharp(SymbolTable table, TextWriter writer)
		{
			List<SymbolTable> nonRootTables = table.GetChildrenTablesRecursive();


			writer.WriteLine(indent() + "protected override void OnApplyBinding(WBinding bindingToApply)");
			writer.WriteLine(indent() + "{");
			depth += 1;
			{
				Symbol rootScopeSymbol = table.GetSymbol(table.CurrentScope);
				writer.WriteLine(indent() + "if(bindingToApply.ScopeBoundTo == " + rootScopeSymbol.ToString() + "){");
				depth += 1;
				{
					CompileTable(table, writer);
				}
				depth -= 1;

				for (int i = 0; i < nonRootTables.Count; ++i)
				{

					Symbol childScopeSymbol = nonRootTables[i].GetSymbol(nonRootTables[i].CurrentScope);
					writer.WriteLine(indent() + "}else if(bindingToApply.ScopeBoundTo == " + childScopeSymbol.ToString() + "){");
					depth += 1;
					{
						CompileTable(nonRootTables[i], writer);
					}
					depth -= 1;
					if (i == nonRootTables.Count - 1)
						writer.WriteLine(indent() + "}");
				}
			}
			depth -= 1;
			writer.WriteLine(indent() + "}");

			return null;
		}

	}


	public abstract class CSharpExpression
	{
		public System.Type Type;
		public bool IsNullable;
		public bool IsIterable;
		public bool IsInferred;
		public bool ShouldSkipNullCheck;

		public CSharpExpression(System.Type t)
		{
			Type = t;
			IsNullable = !t.IsValueType;
			IsIterable = typeof(IEnumerable).IsAssignableFrom(t) || t.IsArray;
		}

		public CSharpExpression() { }

		public abstract string ToExpressionString();
		public abstract string ToNullCheckString();

		public string GetDefaultValue()
		{
			if (Type == null) return "false"; // No default value!

			if (Type.IsValueType)
				return System.Activator.CreateInstance(Type).ToString();

			return "null";
		}
	}
	public class CSharpLiteralExpression : CSharpExpression
	{
		public string LiteralValue;
		public bool IsNull;

		public CSharpLiteralExpression(string literalAsString, System.Type type)
			: base(type)
		{
			LiteralValue = literalAsString;
		}
		public CSharpLiteralExpression()
		{
			IsNull = true;
		}

		public override string ToExpressionString()
		{
			if (IsNull)
				return GetDefaultValue();

			return LiteralValue;
		}

		public override string ToNullCheckString()
		{
			return "true";
		}
	}
	public class CSharpVarExpression : CSharpExpression
	{
		public string Name;
		public CSharpExpression Parent;

		public CSharpVarExpression(string name, System.Type type)
			: base(type)
		{
			Name = name;
		}

		public CSharpVarExpression(string name, System.Type type, CSharpExpression expr)
			: base(type)
		{
			Name = name;
			Parent = expr;
		}

		public override string ToExpressionString()
		{


			if (Parent != null)
			{

				if (IsInferred)
				{
					return "((" + Type.Name + ")" + Parent.ToExpressionString() + ".GetFixedProp(\"" + Name + "\"))";
				}
				else
				{
					return Parent.ToExpressionString() + "." + Name + "/* " + Type + " */";
				}

			}

			return Name;
		}

		public override string ToNullCheckString()
		{
			if (IsNullable)
			{
				if (Parent != null)
				{
					if (ShouldSkipNullCheck)
						return Parent.ToNullCheckString();
					return Parent.ToNullCheckString() + " && " + ToExpressionString() + " != null";
				}

				if (ShouldSkipNullCheck)
					return "";

				return Name + " != null";
			}
			else
			{
				if (Parent != null)
				{
					//Debug.Log("Non nullable! " + Type);
					return Parent.ToNullCheckString() + " /* non nullable */";
				}

				return "";
			}
		}
	}
	public class CSharpArgListExpression : CSharpExpression
	{
		public List<CSharpExpression> ArgExpressions;

		public CSharpArgListExpression(List<CSharpExpression> expressions)
		{
			ArgExpressions = expressions;
		}

		public override string ToNullCheckString()
		{
			string null_check = "";
			int i = 0;
			string argTypes = "";
			foreach (CSharpExpression e in ArgExpressions)
			{
				null_check += e.ToNullCheckString() + " /* " + e.Type + " */";
				if (i < ArgExpressions.Count - 1)
				{
					null_check += " && ";
				}
				i++;
				argTypes += " " + e.Type;
			}


			return null_check + " /* " + argTypes + " */";
		}

		public override string ToExpressionString()
		{
			string expr = "";
			int i = 0;
			foreach (CSharpExpression e in ArgExpressions)
			{
				expr += e.ToExpressionString();
				if (i < ArgExpressions.Count - 1)
				{
					expr += ", ";
				}
				i++;
			}

			return expr;
		}
	}
	public class CSharpFuncExpression : CSharpExpression
	{
		public string Name;
		public CSharpArgListExpression Args;
		public CSharpExpression Parent;
		public int ArgCount;

		public CSharpFuncExpression(string name, System.Type returnType, int argCount, CSharpExpression expr, CSharpArgListExpression argList)
			: base(returnType)
		{
			ArgCount = argCount;
			Args = argList;
			Name = name;
			Parent = expr;
		}

		public override string ToExpressionString()
		{
			return Parent.ToExpressionString() + "(" + Args.ToExpressionString() + ")";
		}

		public override string ToNullCheckString()
		{
			string null_check = Parent.ToNullCheckString() + " /* Func - " + Name + " */";

			if (ArgCount > 0)
				null_check += " && " + Args.ToNullCheckString();
			if (IsNullable)
				null_check += " && " + ToExpressionString() + " != null ";

			return null_check;
		}
	}
	public class CSharpBinOpExpression : CSharpExpression
	{
		public CSharpExpression Op1;
		public CSharpExpression Op2;
		public string Op;

		public CSharpBinOpExpression(CSharpExpression op1, CSharpExpression op2, string op)
			: base(op1.Type)
		{
			Op1 = op1;
			Op2 = op2;
			Op = op;
		}

		public override string ToExpressionString()
		{
			return "(" + Op1.ToExpressionString() + ") " + Op + " (" + Op2.ToExpressionString() + ")";
		}

		public override string ToNullCheckString()
		{
			return Op1.ToNullCheckString() + " && " + Op2.ToNullCheckString();
		}
	}
	public class CSharpUnhandledExpression : CSharpExpression
	{
		public string ExprsesionType;
		public string Message;

		public CSharpUnhandledExpression(string type, string message)
		{
			ExprsesionType = type;
			Message = message;
		}

		public override string ToExpressionString()
		{
			return "/* Unhandled Expression: " + Message + "*/";
		}
		public override string ToNullCheckString()
		{
			return "/* " + ExprsesionType + " */";
		}
	}

}