using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Wrangular.Compiler;

public class WCompiler 
{
	SymbolTable _table;

	public static SymbolTable GenerateSymbolTableFor(WScope scope)
	{	
		SymbolTable _table = new SymbolTable();
		_table.CurrentScope = scope;
		
		WApp.Get.RegisterScopeProperties(scope);

		WModuleBase[] modules = scope.transform.TraverseDownFor<WModuleBase>();
		modules = modules.OrderBy(x => x.GetType().Name).ToArray();

		WScope parentScope = scope.ParentScope;
		while (parentScope != null)
		{
			parentScope = parentScope.ParentScope;
		}
		parentScope = scope.ParentScope;
		
		foreach (WModuleBase module in modules)
		{
			module.AcquireScope();
			if (module.Scope != scope)
				continue;
			
			string expr = module.GetExpression();
			
			List<string> exprs = WScope.ParseExpressions(expr);
			int i = 0;
			foreach(string parsed_exprs in exprs)
			{
				Symbol s = _table.NewSymbol(SymbolTable.MakeSymbolName(module) + "_" + i,
				                            module.GetType().Name,
				                            module.transform);
				s.Expression = parsed_exprs;
				i++;
			}
		}
		
		WScope[] childrenScopes = scope.transform.TraverseDownFor<WScope>(true);
		TraverseChildren(childrenScopes, new List<WScope>(), null, _table);

		return _table;
	}

	protected virtual void Compile(WScope scope, TextWriter sw, string className)
	{
		/*
		 * Wrangular Problem:
		 * Can't access Fixed Properties from compiled c# code
		 * These properties don't exist until runtime (Have to trust they exist)
		 * 
		 * Solution:
		 * 1) Identify these dynamic properties in the CSharpExpression
		 * 2) Use the GetFixedProp method to obtain their vales
		 * 
		 */ 

		if (scope is CompiledControllerBase)
			return;

		_table = new SymbolTable();
		_table.CurrentScope = scope;

		WApp.Get.RegisterScopeProperties(scope);

		sw.WriteLine("using UnityEngine;");
		sw.WriteLine("using System.Collections;");
		sw.WriteLine("using System.Collections.Generic;");
		sw.WriteLine("using System.Linq;");
		sw.WriteLine("using System;");
		sw.WriteLine();

		sw.WriteLine("// Auto Generated Class by Wrangular");

		WModuleBase[] modules = scope.transform.TraverseDownFor<WModuleBase>();
		modules = modules.OrderBy(x => x.GetType().Name).ToArray();


		AwakeNode awake = new AwakeNode();
		ApplyBindingNode applyNode = new ApplyBindingNode();

		WScope parentScope = scope.ParentScope;
		while (parentScope != null)
		{
			Symbol s = _table.NewScopeSymbol(SymbolTable.MakeSymbolName(parentScope),
										parentScope.GetType().Name,
										parentScope.transform);

			awake.AddToVars(new SymbolDeclareNode(s.Name, s.Type));
			parentScope = parentScope.ParentScope;
		}
		parentScope = scope.ParentScope;

		foreach (WModuleBase module in modules)
		{
			module.AcquireScope();
			if (module.Scope != scope)
				continue;

			string expr = module.GetExpression();

			List<string> exprs = WScope.ParseExpressions(expr);
			int i = 0;
			foreach(string parsed_exprs in exprs)
			{
				Symbol s = _table.NewSymbol(SymbolTable.MakeSymbolName(module) + "_" + i,
											module.GetType().Name,
											module.transform);
				s.Expression = parsed_exprs;
				i++;
			}
		}

		WScope[] childrenScopes = scope.transform.TraverseDownFor<WScope>(true);
		TraverseChildren(childrenScopes, new List<WScope>(), awake, _table);

		ClassNode classNode = new ClassNode(className);
		
		// Can change the ordering of the way the script is laid out here
		classNode.AddToClassDefinition(awake);
		classNode.AddToClassDefinition(applyNode);

		classNode.CompileCSharp(_table, sw);

	}
	
	/// <summary>
	/// Recursively make symbol tables for each child scope and their children
	/// </summary>
	protected static void TraverseChildren(WScope[] childrenScopes, List<WScope> visited, AwakeNode awake, SymbolTable parentTable)
	{
		foreach (WScope s in childrenScopes)
		{
			if (visited.Contains(s))
				continue;

			if (s is CompiledControllerBase)
				continue;


			WApp.Get.RegisterScopeProperties(s);

			SymbolTable childTable = parentTable.NewChildTable(s);

			Symbol scope_repeater = parentTable.NewScopeSymbol(SymbolTable.MakeSymbolName(s),
				s.GetType().Name,
				s.transform
				);

			if(awake != null)
				awake.AddToVars(new SymbolDeclareUsingDiffTableNode(scope_repeater, childTable));


			WModuleBase[] child_modules = s.transform.TraverseDownFor<WModuleBase>();
			int i = 0;
			foreach (WModuleBase module in child_modules)
			{
				module.AcquireScope();
				if (module.Scope != s)
					continue;

				string expr = module.GetExpression();

				List<string> exprs = WScope.ParseExpressions(expr);
				foreach (string parsed_exprs in exprs)
				{
					Symbol sym = childTable.NewSymbol(SymbolTable.MakeSymbolName(module) + "_" + i,
												module.GetType().Name,
												module.transform);
					sym.Expression = parsed_exprs;
					i++;
				}
			}

			if (s is WRepeater)
			{
				WRepeater repeater = (WRepeater)s;
				scope_repeater.Expression = repeater.Value;
				CSharpExpression repeater_expr = repeater
					.Generate(scope_repeater.Expression)
					.GenerateCSharp(childTable);

				// This way if child repeaters try to use Item in their repeater value expression,
				// the compiler can infer what type it is and cast it appropriately
					
				if(repeater_expr.Type.IsGenericType)
				{
					// If its generic, assume the type we want is the first one.
					childTable.AddInferredType(repeater.RepeaterName, repeater_expr.Type.GetGenericArguments()[0]);
				}
				else if(repeater_expr.Type.IsArray)
				{
					childTable.AddInferredType(repeater.RepeaterName, repeater_expr.Type.GetElementType());
				}
			}

			visited.Add(s);
			TraverseChildren(s.transform.TraverseDownFor<WScope>(true), visited, awake, childTable);
		}
	}

	public virtual void Compile(WScope scope, string className = null)
	{
		if (scope is CompiledControllerBase)
			return;
		string fileName = "Compiled" + scope.GetType().Name + ".cs";
		if (!string.IsNullOrEmpty(className))
			fileName = className + ".cs";
#if UNITY_EDITOR

		string file_path = "Assets/Scripts/UI/Wrangular/Controllers/Compiled/" + fileName;

		using (StringWriter sw = new StringWriter())
		{
			try
			{
				Compile(scope, sw, className);
				Debug.Log("Compiled " + scope + " .. writing to '" + file_path + "'");
				using (StreamWriter stream = new StreamWriter(file_path))
				{
					stream.Write(sw.GetStringBuilder().ToString());
				}
			}catch(System.Exception e)
			{
				Debug.LogError("Error while compiling " + scope);
				Debug.LogException(e);
			}

		}

		

		UnityEditor.AssetDatabase.ImportAsset("Assets/Scripts/UI/Wrangular/Controllers/Compiled/" + fileName);

		/*
		WScope[] childrenScopes = scope.transform.TraverseDownFor<WScope>(true);

		int scopeNum = 0;
		foreach (WScope s in childrenScopes)
		{
			if (!(s is WRepeater))
			{
				string newClassName = className + "_"
					+ s.GetType().Name + "__"
					+ s.name.Replace(" ", "").Replace("-", "")
					+ "_"
					+ scopeNum;
				Compile(s, newClassName);
			}
		}*/

#endif
	}

}
