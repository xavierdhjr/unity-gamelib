﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class WApplyWhenTrue : WScope 
{
	public string BoolExpression;

	[SerializeField]
	bool _evaluate;

	protected override void OnScopeReady ()
	{
		ParentScope.Bind(this, "{" + BoolExpression + "}", new WBindingSettings()
		                 {
			Apply = OnApply,
			PostApply = OnPostApply
		});
	}

	protected override void FixedUpdate ()
	{

	}


	void OnApply(string propName, object val)
	{
		if(val == null)
		{
			_evaluate = false;
			return;
		}

		if(val is bool)
		{
			if((bool)val)
			{
				_evaluate = true;
			}else{
				_evaluate = false;
			}
		}else{
			_evaluate = false;
		}
	}

	void OnPostApply()
	{
		if(_evaluate)
			OnFixedUpdate();
	}
}
