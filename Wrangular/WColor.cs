﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class WColor : WModuleBase 
{
	
	public override bool IsExpressionMultipleExpressions {
		get {
			return false;
		}
	}

	public string Expression;
    Graphic _label;

	void Awake()
    {
        _label = GetComponent<Graphic>();
		if (_label == null)
            throw new System.Exception("[WColor] No graphic found on " + gameObject);
	}

	protected override void Apply(string propName, object val)
    {
        _label = GetComponent<Graphic>();
		if(val is Color)
		{
			Color c = (Color)val;

            _label.color = new Color(c.r, c.g, c.b, c.a); //_label.color.WithRGB((int)(c.r * 255), (int)(c.g * 255), (int)(c.b * 255));
		}
	}

	protected override string ExpressionString()
	{
		return "{" + Expression + "}";
	}
}
