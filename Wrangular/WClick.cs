﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class WClick : WModuleBase
{
	
	public override bool IsExpressionMultipleExpressions {
		get {
			return false;
		}
	}

	float baseItemAddRate = 0.8f;
	float nextItemAddRate = 1;
	FrameTimer _timer;
	bool _pressed;
    bool _absorbNextClick;

	WExpression _expr;
	PlatformInput _input;
	bool ready;

	public string OnClick;

	protected override void Start()
	{
		AcquireScope();
		CreateBindSettings();
		_expr = Scope.Generate(OnClick);
		
		_timer = new FrameTimer(baseItemAddRate);
		nextItemAddRate = baseItemAddRate * 0.2f;
		_input = InputSystem.Get.GetPlayerInput ();
		ready = true;
	}

	protected override string ExpressionString()
	{
		return "{" + OnClick + "}";
	}

	public virtual void Click()
	{
		if(!ready)
			return;


        // If this click module is part of a list, its value could be changed
        // and the click would be unintentional.
        if (_absorbNextClick)
        {
            _absorbNextClick = false;
            return;
        }

		_expr.Evaluate();
	}

	public virtual void Press()
	{
		if(!ready)
			return;
		_pressed = true;
	}

	public virtual void Release()
	{
		if(!ready)
			return;
		_pressed = false;
	}

    void OnChildValueChanged(WRepeater repeater)
    {
        if (_pressed)
        {
            _absorbNextClick = true;
        }
        _pressed = false;
    }

	protected virtual void Update()
	{

		if(_pressed && !_input.Dragging())
		{
			if(!_timer.NextFrame(Time.deltaTime))
			{
				_expr.Evaluate();
				if(nextItemAddRate > 0.3)
				nextItemAddRate *= 0.95f;
				_timer = new FrameTimer(nextItemAddRate);
			}
		}else{
			
			_timer = new FrameTimer(baseItemAddRate);
			nextItemAddRate = baseItemAddRate * 0.2f;
		}

	}
	#region implemented abstract members of WModuleBase
	
	protected override void Apply (string propName, object val)
	{
	}
	
	#endregion

	void OnDestroy()
	{
		_pressed = false;
	}
}
