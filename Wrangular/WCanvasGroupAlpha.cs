﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class WCanvasGroupAlpha : WModuleBase
{
    public override bool IsExpressionMultipleExpressions
    {
        get
        {
            return false;
        }
    }

    public string Expression;
    public float AlphaIfTrue;
    public float AlphaIfFalse;

    CanvasGroup _group;
    bool _b;

    void Awake()
    {
        _group = GetComponent<CanvasGroup>();
    }

    protected override void Apply(string propName, object val)
    {
        _group = GetComponent<CanvasGroup>();
        if (val is bool)
        {
            bool b = (bool)val;

            _b = b;

            if(_b)
            {
                _group.alpha = AlphaIfTrue;
            }
            else
            {
                _group.alpha = AlphaIfFalse;
            }
        }
    }

    protected override string ExpressionString()
    {
        return "{" + Expression + "}";
    }
}

