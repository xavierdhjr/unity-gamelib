﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class WColorLerp : WModuleBase 
{
    public Color ColorA;
    public Color ColorB;
    public string PercentExpression;

    public override bool IsExpressionMultipleExpressions
    {
        get { return false; }
    }

    protected override string ExpressionString()
    {
        return "{" + PercentExpression + "}";
    }

    protected override void Apply(string propName, object val)
    {
        if (val is float)
        {
            float percent = (float)val;
            GetComponent<Graphic>().color = Color.Lerp(ColorA, ColorB, percent);
        }
    }
}
