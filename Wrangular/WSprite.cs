﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class SpriteSystem
{
    public static SpriteSystem Get
    {
        get
        {
            if (_system == null)
                _system = new SpriteSystem();
            return _system;
        }
    }

    static SpriteSystem _system;

    private class LoadedSprite
    {
        public string Name;
        public Sprite Sprite;
    }

    List<LoadedSprite> _sprites = new List<LoadedSprite>();

    public Sprite GetSprite(string spriteName)
    {
        for (int i = 0; i < _sprites.Count; ++i)
        {
            if (_sprites[i].Name == spriteName)
                return _sprites[i].Sprite;
        }


        if (LoadAndCacheSprite(spriteName))
            return GetSprite(spriteName);
        else
            throw new System.Exception("Could not load sprite '" + spriteName + "'.");

    }

    bool LoadAndCacheSprite(string spriteName)
    {
        LoadedSprite loadedSprite = new LoadedSprite();

        Object[] objs = Resources.LoadAll(spriteName, typeof(Sprite));
        if (objs.Length <= 0)
            return false;

        Object o = objs[0];
        if (o == null || !(o is Sprite))
            return false;

        Sprite s = (Sprite)o;

        loadedSprite.Sprite = s;
        loadedSprite.Name = spriteName;
        _sprites.Add(loadedSprite);

        return true;
    }
}

public class WSprite : WModuleBase 
{
	
	public override bool IsExpressionMultipleExpressions {
		get {
			return true;
		}
	}

	[HideInInspector]
	public Image sprite;
	public string Value;
	Dictionary<string, object> _vals;
	bool _needPostApply;

	protected override void Start()
	{
		base.Start();
        sprite = GetComponent<Image>();
	}

	protected override string ExpressionString ()
	{
		return Value;
	}

	protected override WBindingSettings CreateBindSettings ()
	{
		WBindingSettings settings = base.CreateBindSettings ();
		settings.BindSuccess = OnSuccess;
		settings.PostApply = OnPostApply;
		return settings;
	}

    void OnSuccess(Dictionary<string, object> vals)
    {
        _vals = vals;
    }

	protected override void Apply(string propName, object val)
	{
		if (!BindSettings.ShouldEvaluate)
		{
			return;
		}

		if(_vals.ContainsKey(propName))
		{
			if(_vals[propName] != val)
			{
				_vals[propName] = val;
				_needPostApply = true;
			}
		}
	}

	void OnPostApply()
	{
		
		BindSettings.ShouldEvaluate = true;
		
		// Dont evaluate the sprite expression if it's not even going to be shown
		if(!sprite.gameObject.activeInHierarchy || sprite.color.a <= 0)
		{
			BindSettings.ShouldEvaluate = false;
		}

		if(_needPostApply)
		{
			_needPostApply = false;
			string outputText = Value;
			
			foreach(KeyValuePair<string, object> kvp in _vals)
			{
				string icon_val = "ICON_0";
				
				if(kvp.Value != null)
					icon_val = kvp.Value.ToString();
				outputText = outputText.Replace("{" + kvp.Key + "}", icon_val);


			}
			//sprite.spriteName = outputText;

            if (outputText == "")
                return;

            sprite.sprite = SpriteSystem.Get.GetSprite(outputText); //(Sprite)Resources.Load("Icons/Items/" + outputText);
           
            
		}

	}
}
