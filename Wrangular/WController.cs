﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

public abstract class WController : WScope
{
	protected abstract void OnControllerSetup();


	protected override void OnScopeReady()
	{
		OnControllerSetup();
	}
}
