﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class WDisabled : WModuleBase 
{
    [Wrangular.Attributes.Returns(typeof(bool))]
    public string Expression;

    public override bool IsExpressionMultipleExpressions
    {
        get { return false; }
    }

    protected override string ExpressionString()
    {
        return "{" + Expression + "}";
    }

    protected override void Apply(string propName, object val)
    {
        bool button_enabled = (bool)val;

        GetComponent<Button>().interactable = button_enabled;
    }
}
