﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;

public class WTest : WScope 
{
	public class TestObject
	{
		public string Property { get { return "hi"; } }
	}

	public string GetId() { return "beans123"; }
	public void DoThing() {  }
	public string GetIdWithParams(int a, int b) { return (a + b).ToString(); }
	public string Beans { get { return "beans"; } }
	public TestObject Test { get { return new TestObject(); } }

	public bool A = true;
	public bool B = false;

	void Awake()
	{
		
	}

	protected override void OnScopeReady()
	{
		WExpression funcExprWithRetVal = Generate("GetId()");
		WExpression property = Generate("Beans");
		WExpression field = Generate("B");
		WExpression multiAccessProperty = Generate("Test.Property");
		WExpression booleanExpr = Generate("A && B");
		WExpression funcWithNoRetOrParams = Generate("DoThing()");
		WExpression funcWithParams = Generate("GetIdWithParams(1,2)");

		int testToRun = 100000;

		Stopwatch watch = new Stopwatch();
		watch.Start();
		for (int i = 0; i < testToRun; i++)
		{
			funcExprWithRetVal.Evaluate();
		}
		watch.Stop();
		UnityEngine.Debug.Log("Func expr took: " + watch.ElapsedMilliseconds + "ms");

		watch.Reset();
		watch.Start();
		for (int i = 0; i < testToRun; i++)
		{
			property.Evaluate();
		}
		watch.Stop();
		UnityEngine.Debug.Log("Property expr took: " + watch.ElapsedMilliseconds + "ms");

		watch.Reset();
		watch.Start();
		for (int i = 0; i < testToRun; i++)
		{
			field.Evaluate();
		}
		watch.Stop();
		UnityEngine.Debug.Log("Field expr took: " + watch.ElapsedMilliseconds + "ms");


		watch.Reset();
		watch.Start();
		for (int i = 0; i < testToRun; i++)
		{
			multiAccessProperty.Evaluate();
		}
		watch.Stop();
		UnityEngine.Debug.Log("Multi access expr took: " + watch.ElapsedMilliseconds + "ms");
		
		watch.Reset();
		watch.Start();
		for (int i = 0; i < testToRun; i++)
		{
			booleanExpr.Evaluate();
		}
		watch.Stop();
		UnityEngine.Debug.Log("Boolean expr took: " + watch.ElapsedMilliseconds + "ms");

		watch.Reset();
		watch.Start();
		for (int i = 0; i < testToRun; i++)
		{
			funcWithNoRetOrParams.Evaluate();
		}
		watch.Stop();
		UnityEngine.Debug.Log("Func with no params or retval expr took: " + watch.ElapsedMilliseconds + "ms");

		watch.Reset();
		watch.Start();
		for (int i = 0; i < testToRun; i++)
		{
			funcWithParams.Evaluate();
		}
		watch.Stop();
		UnityEngine.Debug.Log("Func with params expr took: " + watch.ElapsedMilliseconds + "ms");
	}

}
