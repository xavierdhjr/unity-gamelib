﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine.UI;


public class WModel : WModuleBase
{
	public bool CacheFirstNonEmptyString;

	[Wrangular.Attributes.Returns(typeof(string))]
	public string Model;
	Dictionary<string, object> _vals;
	bool _needPostApply;

	public override bool IsExpressionMultipleExpressions {
		get {
			return true;
		}
	}

	protected override void Start()
	{
		// If this object is cloned...
		if(!string.IsNullOrEmpty(Model))
			GetComponent<Text>().text = Model;

        Model = GetComponent<Text>().text; // {Item.PurchasableName}
        _needPostApply = true;
		base.Start();
		
	}

	protected override string ExpressionString ()
	{
		if (!string.IsNullOrEmpty(Model))
			return Model;
		else
            return GetComponent<Text>().text;
	}

	protected override WBindingSettings CreateBindSettings ()
	{
		WBindingSettings settings = base.CreateBindSettings ();
		settings.BindSuccess = OnSuccess;
		settings.PostApply = OnPostApply;
		return settings;
	}

    protected virtual void OnSuccess(Dictionary<string, object> vals)
    {
        _vals = vals;
    }

	protected override void Apply(string propName, object val)
	{
		if (!BindSettings.ShouldEvaluate)
			return;
		if(_vals.ContainsKey(propName))
		{
			if(_vals[propName] != val)
			{
				_vals[propName] = val;
				_needPostApply = true;
			}


		}


	}

	void OnPostApply()
	{
		if(_needPostApply)
		{
			_needPostApply = false;
			string outputText = Model;
			bool anyNulls = false;
			
			foreach(KeyValuePair<string, object> kvp in _vals)
			{
				string v = "NULL";
				if (kvp.Value != null)
				{
					v = kvp.Value.ToString();
					if (CacheFirstNonEmptyString && string.IsNullOrEmpty(v)) anyNulls = true;
				}
				else
					anyNulls = true;
				
				string output = v;
				//output = "Darude - Sandstorm";
				
				outputText = outputText.Replace("{" + kvp.Key + "}", output);
			}

            GetComponent<Text>().text = outputText;
			
			if (CacheFirstNonEmptyString && !anyNulls)
			{
				BindSettings.ShouldEvaluate = false;
			}
		}
	}
}
