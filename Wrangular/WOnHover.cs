﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class WOnHover : WModuleBase
{
    public string OnHoverExpression;

    public override bool IsExpressionMultipleExpressions
    {
        get { return false; }
    }

    protected override string ExpressionString()
    {
        return OnHoverExpression;
    }

    WExpression _onHover;
    bool _didEval;

    protected override void Start()
    {
        AcquireScope();
        _onHover = Scope.Generate(GetExpression());
    }

    protected override void Apply(string propName, object val)
    {

    }

    void Update()
    {
        GameObject obj = InputContextSystem.GetHoverForContext(InputContextSystem.CONTEXT_GUI);

        if (obj == this.gameObject)
        {
            if (!_didEval)
            {
                _onHover.Evaluate();
            }
            _didEval = true;
        }
        else
        {
            _didEval = false;
        }
    }
}

