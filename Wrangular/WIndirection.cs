﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Creates a new property that is read under a given name
/// </summary>
public class WIndirection : WScope 
{
    public string VariableExpression;
    public string NewVariableName;

    public override void OnScopePropertySetup(WApp app, WScope parent)
    {
        AddFixedProp(NewVariableName, null);
        base.OnScopePropertySetup(app, parent);
    }

    protected override void OnScopeReady()
    {
        ParentScope.Bind(this, "{" + VariableExpression + "}", new WBindingSettings()
        {
            Apply = Apply,
            ForceApply = false
        });
    }

    void Apply(string propName, object val)
    {
        AddFixedProp(NewVariableName, val);
    }


}
