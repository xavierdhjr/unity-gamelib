﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class WHide : WModuleBase 
{
	
	public override bool IsExpressionMultipleExpressions {
		get {
			return false;
		}
	}

	public string Value;

    protected override void Start()
	{
		base.Start();
		BindSettings.ForceApply = true;
	}

	protected override string ExpressionString ()
	{
		return "{" + Value + "}";
	}
	
	protected override void Apply(string propName, object val)
	{
		bool isBool = val is bool;

		// This will need an update every frame
		if(!isBool)
		{
			gameObject.SetActive(false);
			return;
		}

		bool b = (bool)val;

		if (b == gameObject.activeSelf)
			return;

		gameObject.SetActive(b);

	}

}
