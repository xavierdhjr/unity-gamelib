﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public class BaseController : WController 
{

	public GameUI ControllerGameUI { get { return _ui; } }

	GameUI _ui;
	bool _hasUI = false;
    List<IEnumerator> _highPriorityJobs = new List<IEnumerator>();

    public bool not(bool a)
    {
        return !a;
    }

    protected void AddHighPriorityJob(IEnumerator job)
    {
        _highPriorityJobs.Add(job);
    }

	protected override void OnControllerSetup ()
	{
		_ui = GetComponent<GameUI>();
		_hasUI = _ui != null;
	}

    protected void ClearAllJobs()
    {
        _highPriorityJobs = new List<IEnumerator>();
    }

	protected virtual void Update()
	{
		if (_hasUI)
		{
			SetReady(_ui.Shown);
		}

        for(int i = 0; i < _highPriorityJobs.Count; ++i)
        {
            if(!_highPriorityJobs[i].MoveNext())
            {
                _highPriorityJobs.RemoveAt(i);
                i--;
            }
        }
	}



}
