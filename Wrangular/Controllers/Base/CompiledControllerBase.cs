﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public abstract class CompiledControllerBase : WScope
{
	protected abstract void OnReady();
	protected virtual void OnScopeUpdate(){ }

	protected override void OnScopeReady()
	{
		WScope[] scopes = GetComponents<WScope>();

		for (int i = 0; i < scopes.Length; ++i )
		{
			if (scopes[i] == this)
				continue;
			else
			{
				scopes[i].GetHijackedBy(this);
				break;
			}
		}

		OnReady();
	}

	protected override void FixedUpdate()
	{
		base.FixedUpdate();
		// Do nothing! 
	}

	protected void Update()
	{
		if(Ready)
			OnScopeUpdate();
	}
}
