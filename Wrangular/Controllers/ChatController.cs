﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ChatController : BaseController 
{
    public class ChatMessage
    {
        public enum Type
        {
            LevelUp,
            Normal,
            Event
        }

        public string Message;
        public string Sender;
        public Type MessageType;

        public const string LEVEL_UP_COLOR = "#00ff00";
        public const string NORMAL_COLOR = "yellow";
        public const string EVENT_COLOR = "white";

        public ChatMessage(string message, string sender, Type messageType)
        {
            Message = message;
            Sender = sender;
            MessageType = messageType;
        }

        public override string ToString()
        {
            if(MessageType == Type.Normal)
                return "<color=" + NORMAL_COLOR + ">" + Sender + ": " + Message + "</color>";

            if (MessageType == Type.LevelUp)
                return "<color=" + LEVEL_UP_COLOR + ">" + Message + "</color>";

            return Message;
        }

    }

    public static ChatController Get
    {
        get
        {
            if (_chat == null)
                _chat = GameObject.FindObjectOfType<ChatController>();
            return _chat;
        }
    }

    static ChatController _chat;

    public int MessageLimit = 100;
    public List<ChatMessage> Messages = new List<ChatMessage>();

    bool _cacheValid;
    string _cachedText = "";

    public string TextNoFormat;
    public bool Show;

    public string Text
    {
        get
        {
            if (!_cacheValid)
            {
                System.Text.StringBuilder strBlder = new System.Text.StringBuilder();
                for (int i = 0; i < Messages.Count; ++i)
                {
                    strBlder.AppendLine(Messages[i].ToString());
                }
                _cachedText = strBlder.ToString();
                _cacheValid = true;

                if (Messages.Count == 0)
                    _cachedText = " ";
                System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(@"\<.+\>(?<value>[^<]*)\</.+\>");
                System.Text.RegularExpressions.Match match = reg.Match(_cachedText);
                TextNoFormat = _cachedText;
                while(match.Success)
                {
                    TextNoFormat = reg.Replace(TextNoFormat, match.Groups["value"].Value, 1);
                    match = reg.Match(TextNoFormat);
                }
            }
            return _cachedText;
        }
    }

    public void AddMessage(string message, string sender, ChatMessage.Type type)
    {
        if(Messages.Count >= MessageLimit)
        {
            Messages.RemoveAt(0);
        }
        Messages.Add(new ChatMessage(message, sender, type));
        _cacheValid = false;
    }

    public void ClearChat()
    {
        Messages = new List<ChatMessage>();
        _cacheValid = false;
    }



}
